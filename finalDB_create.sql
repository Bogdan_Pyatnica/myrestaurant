-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema myDB
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema myDB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `myDB` DEFAULT CHARACTER SET utf8 ;
USE `myDB` ;

-- -----------------------------------------------------
-- Table `myDB`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myDB`.`role` ;

CREATE TABLE IF NOT EXISTS `myDB`.`role` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myDB`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myDB`.`user` ;

CREATE TABLE IF NOT EXISTS `myDB`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `user_name` VARCHAR(45) NULL,
  `role_id` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  INDEX `fk_user_role_idx` (`role_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_role`
    FOREIGN KEY (`role_id`)
    REFERENCES `myDB`.`role` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myDB`.`status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myDB`.`status` ;

CREATE TABLE IF NOT EXISTS `myDB`.`status` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT 'ORDERED',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myDB`.`receipt`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myDB`.`receipt` ;

CREATE TABLE IF NOT EXISTS `myDB`.`receipt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `total` INT UNSIGNED NULL,
  `create_date` TIMESTAMP NULL,
  `update_date` TIMESTAMP NULL,
  `status_id` INT NULL DEFAULT 1,
  `user_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_receipt_status1_idx` (`status_id` ASC) VISIBLE,
  INDEX `fk_receipt_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_receipt_status1`
    FOREIGN KEY (`status_id`)
    REFERENCES `myDB`.`status` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_receipt_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `myDB`.`user` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myDB`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myDB`.`category` ;

CREATE TABLE IF NOT EXISTS `myDB`.`category` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myDB`.`food`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myDB`.`food` ;

CREATE TABLE IF NOT EXISTS `myDB`.`food` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NULL,
  `name` VARCHAR(45) NOT NULL,
  `price` INT UNSIGNED NOT NULL,
  `amount` INT UNSIGNED NOT NULL,
  `picture` VARCHAR(45) NULL,
  `category_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_food_category1_idx` (`category_id` ASC) VISIBLE,
  CONSTRAINT `fk_food_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `myDB`.`category` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myDB`.`receipt_has_food`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myDB`.`receipt_has_food` ;

CREATE TABLE IF NOT EXISTS `myDB`.`receipt_has_food` (
  `receipt_id` INT NOT NULL,
  `food_id` INT NOT NULL,
  `count` INT NULL,
  `price` INT NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`receipt_id`, `food_id`),
  INDEX `fk_receipt_has_food_food1_idx` (`food_id` ASC) VISIBLE,
  INDEX `fk_receipt_has_food_receipt1_idx` (`receipt_id` ASC) VISIBLE,
  CONSTRAINT `fk_receipt_has_food_receipt1`
    FOREIGN KEY (`receipt_id`)
    REFERENCES `myDB`.`receipt` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_receipt_has_food_food1`
    FOREIGN KEY (`food_id`)
    REFERENCES `myDB`.`food` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

insert into role (id,name) values (1,"user");
insert into role (id, name) values (2,"manager");

insert into category (name) values ("pizza");
insert into category (name) values ("burger");
insert into category (name) values ("drink");
insert into category (name) values ("desert");

insert into status (id,name) values (1,"ordered");
insert into status (id, name) values (2,"is preparing");
insert into status (id, name) values (3,"ready");
insert into status (id, name) values (4,"delivery");
insert into status (id, name) values (5,"accepted");
insert into status (id, name) values (6,"сanceled");


DROP TRIGGER IF EXISTS myDB.bi_receipt_has_food_update_total_price;
DELIMITER //
CREATE DEFINER = CURRENT_USER TRIGGER myDB.bi_receipt_has_food_update_total_price
BEFORE INSERT ON receipt_has_food FOR EACH ROW
BEGIN
    DECLARE newamount int;
	SET newamount = (select food.amount from food where id = NEW.food_id) - NEW.count;
    SET NEW.price = (select price from food where id = NEW.food_id);
     IF (newamount < 0) THEN
		SIGNAL sqlstate '45001' set message_text = "Not enough food!";
    END IF;
        UPDATE food SET food.amount = newamount WHERE id = NEW.food_id;    
        UPDATE receipt SET total = ifnull(total, 0) + NEW.price * NEW.count WHERE receipt.id = NEW.receipt_id;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS myDB.bi_receipt_has_status_removed;
DELIMITER //
CREATE DEFINER = CURRENT_USER TRIGGER myDB.bi_receipt_has_status_removed
AFTER UPDATE ON receipt FOR EACH ROW
BEGIN
DECLARE newstatus int; 
SET newstatus = (SELECT status_id from receipt where id = OLD.id);
IF (newstatus = 6) THEN
		UPDATE receipt_has_food set count=0 WHERE OLD.id = receipt_has_food.receipt_id;
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS myDB.bd_receipt_has_food_delete;
DELIMITER //
CREATE DEFINER = CURRENT_USER TRIGGER myDB.bd_receipt_has_food_delete
BEFORE UPDATE ON receipt_has_food FOR EACH ROW
BEGIN
  UPDATE food SET amount = amount + OLD.count WHERE id = OLD.food_id;
END//
DELIMITER ;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
