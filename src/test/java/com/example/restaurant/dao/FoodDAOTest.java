package com.example.restaurant.dao;

import com.example.restaurant.database.DBManager;
import com.example.restaurant.models.Food;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


class FoodDAOTest {
    DBManager dbManager;
    String testFoodName = "test_food";
    @BeforeEach
    public void getConnection(){
        dbManager= DBManager.getInstance(true);
    }
    @Test
    public void shouldAddFoodToDatabase(){
        Food testFood = Food.createFood();
        testFood.setAmount(1);
        testFood.setPicture("test.jpg");
        testFood.setPrice(111);
        testFood.setCategory(1);
        testFood.setName(testFoodName);
        testFood.setDescription("test_desc");
        testFood.setFoodInBDamount(1);
        FoodDAO.addFoodToDb(testFood);
        Food foodCheck = FoodDAO.getFoodByName(testFood.getName());
        assertEquals(testFood,foodCheck);
    }
    @AfterEach
    public void removeFood(){
        Food foodDelete  = Food.createFood();
        foodDelete.setName(testFoodName);
        FoodDAO.deleteFood(foodDelete);
    }

    @Test
    public void shouldSelectFoodByName(){
        Food testFood = Food.createFood();
        testFood.setAmount(1);
        testFood.setPicture("test.jpg");
        testFood.setPrice(111);
        testFood.setCategory(1);
        testFood.setName(testFoodName);
        testFood.setDescription("test_desc");
        testFood.setFoodInBDamount(1);
        FoodDAO.addFoodToDb(testFood);

        Food foodCheck = FoodDAO.getFoodByName(testFoodName);
        assertEquals(testFood,foodCheck);
    }
    @Test
    public void shouldReturnAllCategory(){
        Map<Integer,String> foodCategory;
        foodCategory = FoodDAO.getAllCategory();
        assertFalse(foodCategory.isEmpty());
    }

    @Test
    public void shouldGetFoodCount(){
        Food testFood = Food.createFood();
        testFood.setAmount(1);
        testFood.setPicture("test.jpg");
        testFood.setPrice(111);
        testFood.setCategory(1);
        testFood.setName(testFoodName);
        testFood.setDescription("test_desc");
        testFood.setFoodInBDamount(1);
        FoodDAO.addFoodToDb(testFood);

        int count = FoodDAO.getFoodCount(1);
        assertTrue(count>=1);
    }
    @Test
    public void shouldGetAllFoodByCategory(){
        List<Food> foodList;

        Food testFood = Food.createFood();
        testFood.setAmount(1);
        testFood.setPicture("test.jpg");
        testFood.setPrice(111);
        testFood.setCategory(1);
        testFood.setName(testFoodName);
        testFood.setDescription("test_desc");
        testFood.setFoodInBDamount(1);
        FoodDAO.addFoodToDb(testFood);

        foodList=FoodDAO.getAllFood(1);
        assertFalse(foodList.isEmpty());
    }

    @Test
    public void shouldEditFood(){
        Food testFood = Food.createFood();
        testFood.setAmount(1);
        testFood.setPicture("test.jpg");
        testFood.setPrice(111);
        testFood.setCategory(1);
        testFood.setName(testFoodName);
        testFood.setDescription("test_desc");
        testFood.setFoodInBDamount(1);

        int amountBefore = testFood.getAmount();
        FoodDAO.addFoodToDb(testFood);

        Food foodFromDB = FoodDAO.getFoodByName(testFoodName);

        foodFromDB.setAmount(2);
        FoodDAO.editFood(foodFromDB);
        Food afterEdit = FoodDAO.getFoodByName(testFoodName);
        int amountAfter = afterEdit.getFoodInBDamount();

        assertTrue(amountBefore<amountAfter);
    }
    @Test
    public void shouldDeleteFood(){
        Food testFood = Food.createFood();
        testFood.setAmount(1);
        testFood.setPicture("test.jpg");
        testFood.setPrice(111);
        testFood.setCategory(1);
        testFood.setName(testFoodName);
        testFood.setDescription("test_desc");
        testFood.setFoodInBDamount(1);
        FoodDAO.addFoodToDb(testFood);

        FoodDAO.deleteFood(testFood);
        Food foodAfter = FoodDAO.getFoodByName(testFoodName);
        assertNotEquals(foodAfter.getName(), testFood.getName());
    }

}