package com.example.restaurant.dao;

import com.example.restaurant.database.DBManager;
import com.example.restaurant.models.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserDAOTest {
    DBManager dbManager;
    String testLogin = "login";

    @BeforeEach
    public void getConnection(){
        dbManager=DBManager.getInstance(true);
    }

    @Test
    public void shouldGetUserByLogin(){
        User user = User.createUser();
        user.setEmail("testuser@gmail.com");
        user.setRole(1);
        user.setName("test_user");
        user.setPassword("test");
        user.setLogin(testLogin);
        UserDAO.insertUser(user);

        User userCheck = UserDAO.getUser(testLogin);
        assertEquals(testLogin, userCheck.getLogin());
    }

    @Test
    public void shouldInsertUser(){
        User user = User.createUser();
        user.setEmail("testuser@gmail.com");
        user.setRole(1);
        user.setName("test_user");
        user.setPassword("test");
        user.setLogin(testLogin);
        UserDAO.insertUser(user);
        User userCheck = UserDAO.getUser(user.getLogin());
        assertEquals(user,userCheck);
    }

    @Test
    public void shouldGetUserList(){
        List<User> userList;
        User user = User.createUser();
        user.setEmail("testuser@gmail.com");
        user.setRole(1);
        user.setName("test_user");
        user.setPassword("test");
        user.setLogin(testLogin);
        UserDAO.insertUser(user);
        userList = UserDAO.getAllUsers();

        assertFalse(userList.isEmpty());
    }

    @AfterEach
    public void removeTestUser(){
        User userCheck = UserDAO.getUser(testLogin);
        if (userCheck.getLogin()!=null){
            UserDAO.deleteUserByLogin(testLogin);
        }
    }

}