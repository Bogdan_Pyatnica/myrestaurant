package com.example.restaurant.dao;

import com.example.restaurant.database.DBManager;
import com.example.restaurant.exceptions.InsertUserException;
import com.example.restaurant.models.Food;
import com.example.restaurant.models.Receipt;
import com.example.restaurant.models.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class ReceiptDAOTest {
    DBManager dbManager;
    String foodNameTest = "test_food";
    String userLoginTest = "test_login";
    @BeforeEach
    public void getConnection(){
        dbManager= DBManager.getInstance(true);
    }
    @BeforeAll
    public static void getConnectionBefore(){
       DBManager dbManager= DBManager.getInstance(true);
    }

    @Test
    public void shouldMakeReceiptForUser() throws InsertUserException {
        Food foodTest = FoodDAO.getFoodByName(foodNameTest);
        User userTest = UserDAO.getUser(userLoginTest);
        Set<Receipt> receiptSet;
        ReceiptDAO.makeReceipt(userTest,foodTest);
        receiptSet= ReceiptDAO.getReceiptFromUser(userTest);
        int receiptId = receiptSet.stream().findFirst().get().getId();
        assertFalse(receiptSet.isEmpty());
        ReceiptDAO.deleteReceiptById(receiptId);
    }
    @Test
    public void shouldGetAllStatuses()  {
        Map<Integer,String> receiptStatuses = ReceiptDAO.getAllReceiptStatus();
        assertFalse(receiptStatuses.isEmpty());
    }
    @Test
    public void shouldReturnAllReceiptByStatus() throws InsertUserException {
        Food foodTest = FoodDAO.getFoodByName(foodNameTest);
        User userTest = UserDAO.getUser(userLoginTest);
        Set<Receipt> receiptSet;
        ReceiptDAO.makeReceipt(userTest,foodTest);
        Map<Integer,String> statuses = ReceiptDAO.getAllReceiptStatus();

        Set<Receipt> getReceipt = ReceiptDAO.getAllReceipt(statuses.get(1));
        assertFalse(getReceipt.isEmpty());

        receiptSet= ReceiptDAO.getReceiptFromUser(userTest);
        int receiptId = receiptSet.stream().findFirst().get().getId();
        ReceiptDAO.deleteReceiptById(receiptId);
    }

    @Test
    public void shouldReturnFoodFromReceipt() throws InsertUserException {
        Food foodTest = FoodDAO.getFoodByName(foodNameTest);
        User userTest = UserDAO.getUser(userLoginTest);
        ReceiptDAO.makeReceipt(userTest,foodTest);
        Set<Receipt> receiptSet = ReceiptDAO.getReceiptFromUser(userTest);
        int receiptId = receiptSet.stream().findFirst().get().getId();
        String foodName = foodNameTest;
        String foodNameFromReceipt = ReceiptDAO.getFoodForReceipt(receiptId).get(0).getName();
        assertEquals(foodName,foodNameFromReceipt);
        ReceiptDAO.deleteReceiptById(receiptId);
    }
    @Test
    public void shouldGetReceiptForUser() throws InsertUserException {
        Food foodTest = FoodDAO.getFoodByName(foodNameTest);
        User userTest = UserDAO.getUser(userLoginTest);
        ReceiptDAO.makeReceipt(userTest,foodTest);
        Set<Receipt> receiptSet = ReceiptDAO.getReceiptFromUser(userTest);
        int receiptId = receiptSet.stream().findFirst().get().getId();
        assertFalse(receiptSet.isEmpty());
        ReceiptDAO.deleteReceiptById(receiptId);
    }
    @Test
    public void shouldGetReceiptForUserByStatus() throws InsertUserException {
        String testStatus = "ordered";
        Food foodTest = FoodDAO.getFoodByName(foodNameTest);
        User userTest = UserDAO.getUser(userLoginTest);
        ReceiptDAO.makeReceipt(userTest,foodTest);
        Set<Receipt> receiptSet = ReceiptDAO.getReceiptFromUser(userTest,testStatus);
        String receiptStatus = receiptSet.stream().findFirst().get().getStatusName();
        int receiptId = receiptSet.stream().findFirst().get().getId();
        assertEquals(testStatus,receiptStatus);
        ReceiptDAO.deleteReceiptById(receiptId);
    }

    @BeforeEach
    public void beforeTests(){
        User user = User.createUser();
        user.setEmail("testuser@gmail.com");
        user.setRole(1);
        user.setName("test_user");
        user.setPassword("test");
        user.setLogin(userLoginTest);
        UserDAO.insertUser(user);

        Food testFood = Food.createFood();
        testFood.setAmount(1);
        testFood.setPicture("test.jpg");
        testFood.setPrice(111);
        testFood.setCategory(1);
        testFood.setName(foodNameTest);
        testFood.setDescription("test_desc");
        testFood.setFoodInBDamount(1);
        FoodDAO.addFoodToDb(testFood);
    }
    @AfterEach
    public void afterTests(){
        Food food = Food.createFood();
        food.setName(foodNameTest);
        FoodDAO.deleteFood(food);
        UserDAO.deleteUserByLogin(userLoginTest);
    }


}