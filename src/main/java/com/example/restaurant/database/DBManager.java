package com.example.restaurant.database;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Database config.
 *
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */

public class DBManager {
    private static DBManager instance;
    private boolean isTest;
    private static final Logger log = LogManager.getLogger(DBManager.class);
    private DBManager(){}
    private DBManager(boolean isTest){
        this.isTest = isTest;
    }
    /**
     * Return the instance of DBManager
     */
    public static DBManager getInstance(){
        if (instance == null){
            instance=new DBManager();
        }
        return instance;
    }
    /**
     * Return the instance of DBManager
     * @param isTest using for get instance without connectionPool. Using for test.
     *
     */
    public static DBManager getInstance(boolean isTest){
        if (instance == null){
            instance=new DBManager(isTest);
        }
        return instance;
    }

    /**
     * Return connection for database according to the isTest value in method getInstance.
     */
    public Connection getConnection(){
        Context context;
        Connection connection = null;
        if (isTest){
            return getConnectionForTests();
        }
        try {
            context = new InitialContext();
            DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/UsersDB");
            connection = ds.getConnection();
            log.info("Get connection by pool");
        } catch (NamingException | SQLException e) {
            log.error("Error while connection pooling ", e);
        }
        return connection;
    }

    /**
     * Return the connection for test database.
     */
    private Connection getConnectionForTests(){
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException | ClassNotFoundException e) {
            log.error("Error while connection for test. ", e);
        }

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/myDBforTest?serverTimezone=Europe/Kiev","root","98256000Bsql5960");
        } catch (SQLException throwables) {
            log.error("Error while getting connection for test. ", throwables);
        }
        return connection;
    }


}
