package com.example.restaurant.dao;

import com.example.restaurant.database.DBManager;
import com.example.restaurant.exceptions.InsertUserException;
import com.example.restaurant.models.Food;
import com.example.restaurant.models.Receipt;
import com.example.restaurant.models.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.*;

/**
 * Receipt data access object.
 * Contains methods for interacting with database
 *
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
public class ReceiptDAO {
    /**
     * All query declared as constance.
     */
    private static final String UPDATE_RECEIPT_STATUS = "update receipt set status_id = (?) where status_id = (?) and receipt.id = (?);";
    private static final String CANCEL_RECEIPT = "update receipt set status_id = 6 where receipt.id = (?);";
    private static final String GET_ALL_STATUS = "SELECT * FROM status;";
    private static final String SET_RECEIPT_FOR_USER="INSERT INTO receipt (status_id,user_id) VALUES (1,?);"; //1 operation
    private static final String SET_FOOD_FOR_RECEIPT="INSERT INTO receipt_has_food (receipt_id,food_id,count) VALUES (?,?,?);"; //2 operation
    private static final String GET_RECEIPT_FROM_USER=
            "SELECT distinct receipt_id, user_id, food_id, food.name, category_id, food.price,receipt.id,receipt_has_food.create_time, total, description,receipt.status_id,status.name,status.id,food.id " +
                    "FROM " +
                    "user, food,  receipt_has_food, receipt,status " +
                    "WHERE " +
                    "receipt.id = receipt_id and food.id = food_id and (?) = user_id and status.id=status_id " +
                    "order by receipt_has_food.create_time desc; ";

    private static final String GET_RECEIPT_FROM_USER2=
            "SELECT distinct receipt_id, user_id, food_id, food.name, category_id, food.price,receipt.id,receipt_has_food.create_time, total, description,receipt.status_id,status.name,status.id,food.id " +
                    "FROM " +
                    "user, food,  receipt_has_food, receipt,status " +
                    "WHERE " +
                    "receipt.id = receipt_id and food.id = food_id and (?) = user_id and status.id=status_id and status.name =(?) " +
                    "order by receipt_has_food.create_time desc; ";
            //3 operation, param USER user.
    private static final String GET_ALL_RECEIPT_BY_STATUS = "SELECT receipt_id, user_id, food_id, food.name, user.login, category_id,receipt_has_food.create_time, total,food.price,status.name, status_id, description " +
            "FROM " +
            "user, food,  receipt_has_food, receipt, status " +
            "WHERE " +
            "receipt.id = receipt_id and food.id = food_id and user.id = user_id and status.id=status_id and status.name=(?) " +
            "order by receipt_has_food.create_time desc; ";
    private static final String GET_FOOD_FOR_RECEIPT="SELECT food_id, receipt_id, food.name, receipt_has_food.price, food.id, count, category_id, food.description " +
            "FROM " +
            "receipt_has_food, food " +
            "where " +
            "receipt_id=(?) and food.id=food_id;";
    private static final String GET_RECEIPT_BY_ID = "SELECT * FROM receipt WHERE id = ?;";
    private static final String DELETE_RECEIPT_BY_ID = "DELETE FROM receipt WHERE id=?";
    //for pagination
    private static final String GET_RECEIPT_COUNT_FOR_USER = "SELECT  count(id) FROM receipt WHERE user_id = (?) AND status_id=(?);";

    /**
     * initialize the logger for this class
     */
    private static final Logger log = LogManager.getLogger(ReceiptDAO.class);

    /**
     * Method helper for pagination.
     * @param userId for userReceipt.
     * @param statusId for receipt status
     */
    public static int getReceiptCountForUser(int userId, int statusId){
        int count = 0;
        Connection connection = null;
        PreparedStatement preparedStatement =null;
        ResultSet resultSet = null;
        try{
            connection = DBManager.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(GET_RECEIPT_COUNT_FOR_USER);
            preparedStatement.setInt(1,userId);
            preparedStatement.setInt(2,statusId);
            resultSet= preparedStatement.executeQuery();
            if (resultSet.next()){
                count=resultSet.getInt(1);
            }
        } catch (SQLException throwables) {
            log.error("Cannot get receipt count for user, id: "+userId+" status id: "+statusId,throwables);
        }finally{
            if (resultSet!=null){
                closeResources(resultSet);
            }
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
        return count;
    }

    /**
     * Delete receipt by id.
     * @param id for finding the right one.
     */
    public static void deleteReceiptById(int id){
        Connection connection = null;
        PreparedStatement preparedStatement =null;
        try{
            connection = DBManager.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(DELETE_RECEIPT_BY_ID);
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            log.error("Cannot delete receipt with id "+id,throwables);
        }finally{
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
    }

    /**
     * Find recipt by id
     * @param receiptId for finding the right one
     * @return receipt with same id as param
     */
    public static Receipt getReceiptById(int receiptId){
        Receipt receipt = Receipt.getReceipt();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement=null;
        try {
            connection=DBManager.getInstance().getConnection();
            preparedStatement=connection.prepareStatement(GET_RECEIPT_BY_ID);
            preparedStatement.setInt(1,receiptId);
            resultSet=preparedStatement.executeQuery();
            while (resultSet.next()){
                receipt.setTotal(resultSet.getInt("total"));
                receipt.setId(resultSet.getInt("id"));
            }
        } catch (SQLException throwables) {
            log.error("Cannot get receipt " + receiptId,throwables);
        } finally {
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
            if (resultSet!=null){
                closeResources(resultSet);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
        return  receipt;
    }

    /**
     * Method for creating receipt for user
     * @param user user which make receipt for
     * @param foods all food in user order
     */
    public static void makeReceipt(User user, Food... foods){
                Connection connection = null;
                Savepoint savePoint = null;
                try{
                    connection = DBManager.getInstance().getConnection();
                    connection.setAutoCommit(false);
                    savePoint = connection.setSavepoint("Point one");
                    connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
                    int receipt_id = setReceiptForUser(connection,user);
                    for (Food food : foods){
                        setFoodForReceipt(receipt_id,connection,food);
                    }
                    connection.commit();
                    connection.setAutoCommit(true);
                } catch (SQLException throwables) {
                    try {
                        connection.rollback(savePoint);
                    } catch (SQLException e) {
                        log.error("Cannot make receipt",e);
                    }
                   log.warn("WRONG DATA",throwables);
                }finally{
                    if (connection!=null){
                        closeResources(connection);
                    }
                }
            }
            public static Map<Integer,String> getAllReceiptStatus(){
                Map<Integer,String> mapStatus = new HashMap<>();
                Connection connection = null;
                Statement statement = null;
                ResultSet resultSet = null;
                try {
                    connection=DBManager.getInstance().getConnection();
                    statement=connection.createStatement();
                    resultSet=statement.executeQuery(GET_ALL_STATUS);
                    while (resultSet.next()){
                        mapStatus.put(resultSet.getInt("id"),resultSet.getString("name"));
                    }
                } catch (SQLException throwables) {
                   log.error("Cannot get statuses for receipt",throwables);
                }finally{
                 if (resultSet!=null){
                     closeResources(resultSet);
                  }
                 if (statement!=null){
                     closeResources(statement);
                  }
                 if (connection!=null){
                     closeResources(connection);
                  }
                }
                return mapStatus;
            }

    /**
     * Method for updating receipt status
     * @param receiptId for finding target receipt
     * @param currentStatusId current status, for gradually change status
     */
            public static void updateReceiptStatus(int receiptId, int currentStatusId){
                Connection connection = null;
                PreparedStatement preparedStatement = null;
                try {
                    connection=DBManager.getInstance().getConnection();
                    preparedStatement=connection.prepareStatement(UPDATE_RECEIPT_STATUS);
                    preparedStatement.setInt(1,currentStatusId+1);
                    preparedStatement.setInt(2,currentStatusId);
                    preparedStatement.setInt(3,receiptId);
                    preparedStatement.executeUpdate();
                    log.info("status has been updated");
                } catch (SQLException throwables) {
                    log.error("Cannot update status",throwables);
                } finally {
                    if (preparedStatement!=null){
                        closeResources(preparedStatement);
                    }
                    if (connection!=null){
                        closeResources(connection);
                    }
                }
            }

    /**
     *  Method for canceling the receipt
     *  Method DO NOT delete receipt from database, only change status
     * @param receiptId target receipt id
     */
            public static void cancelReceipt(int receiptId){
                Connection connection = null;
                PreparedStatement preparedStatement = null;
                try {
                    connection=DBManager.getInstance().getConnection();
                    preparedStatement=connection.prepareStatement(CANCEL_RECEIPT);
                    preparedStatement.setInt(1,receiptId);
                    preparedStatement.executeUpdate();
                } catch (SQLException throwables) {
                    log.error("Cannot cancel receipt "+receiptId,throwables);
                } finally {
                    if (preparedStatement!=null){
                        closeResources(preparedStatement);
                    }
                    if (connection!=null){
                        closeResources(connection);
                    }
                }
            }

    /**
     * Method fro getting list of food for target receipt
     * @param receiptId target receipt
     * @return list of food in receipt
     */
    public static List<Food> getFoodForReceipt(int receiptId){
        List<Food> foodList= new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement=null;
        try {
            connection=DBManager.getInstance().getConnection();
            preparedStatement=connection.prepareStatement(GET_FOOD_FOR_RECEIPT);
            preparedStatement.setInt(1,receiptId);
            resultSet=preparedStatement.executeQuery();
            while (resultSet.next()){
                foodList.add(ReceiptDAO.createFood(resultSet));
            }

        } catch (SQLException throwables) {
            log.error("Cannot get food for receipt " + receiptId,throwables);
        } finally {
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
            if (resultSet!=null){
                closeResources(resultSet);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
        return foodList;
    }

    /**
     * Method for getting all receipt be status
     * @param statusName target status
     * @return Set of receipt.
     */
            public static Set<Receipt> getAllReceipt(String statusName){
                Set<Receipt> receiptList = new TreeSet<>();
                Connection connection = null;
                PreparedStatement preparedStatement = null;
                ResultSet resultSet = null;
                try {
                    connection=DBManager.getInstance().getConnection();
                    preparedStatement=connection.prepareStatement(GET_ALL_RECEIPT_BY_STATUS);
                    preparedStatement.setString(1,statusName);
                    resultSet=preparedStatement.executeQuery();
                    while (resultSet.next()){
                        receiptList.add(mapReceipt(resultSet));
                    }
                } catch (SQLException throwables) {
                    log.error("Cannot get all receipt with status: "+statusName,throwables);
                } finally {
                    if (resultSet!=null){
                        closeResources(resultSet);
                    }
                    if (preparedStatement!=null){
                        closeResources(preparedStatement);
                    }
                    if (connection!=null){
                        closeResources(connection);
                    }
                }
                return receiptList;
            }

    /**
     * Method for making receipt
     * @param connection - connection of the query
     * @param user - user for receipt
     * @return - receipt id
     * @throws SQLException
     */
            private static int setReceiptForUser(Connection connection, User user) throws SQLException {
                int receipt_id = 0;
            PreparedStatement preparedStatement = connection.prepareStatement(SET_RECEIPT_FOR_USER,Statement.RETURN_GENERATED_KEYS);
                try {
                    preparedStatement.setInt(1,UserDAO.getUser(user.getLogin()).getId());
                    preparedStatement.executeUpdate();
                    try(ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                        if (resultSet.next()){
                           receipt_id=resultSet.getInt(1);
                        }
                    }
                } finally {
                    closeResources(preparedStatement);
                }
                return receipt_id;
            }

    /**
     *
     * @param receipt_id - id of the receipt
     * @param connection - connection for the query
     * @param food - food in receipt
     * @throws SQLException
     */
            private static void setFoodForReceipt(int receipt_id,Connection connection,Food food) throws SQLException {
                PreparedStatement preparedStatement = connection.prepareStatement(SET_FOOD_FOR_RECEIPT);
                    preparedStatement.setInt(1,receipt_id);
                    preparedStatement.setInt(2,FoodDAO.getFoodByName(food.getName()).getId());
                    preparedStatement.setInt(3,food.getAmount());
                    preparedStatement.executeUpdate();
                    closeResources(preparedStatement);
            }

    /**
     * Method for getting all receipt for specific user
     * @param user - target user
     * @return Set of receipt for this user
     * @throws InsertUserException
     */
    public static Set<Receipt> getReceiptFromUser (User user) throws InsertUserException {
                Set<Receipt> receiptList = new TreeSet<>();
                User userFromDb = UserDAO.getUser(user.getLogin());
                int userId = userFromDb.getId();
                Connection connection=null;
                PreparedStatement preparedStatement = null;
                try {
                connection=DBManager.getInstance().getConnection();
                preparedStatement = connection.prepareStatement(GET_RECEIPT_FROM_USER,PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setInt(1,userId);
                try(ResultSet resultSet = preparedStatement.executeQuery()){
                    while (resultSet.next()){
                        receiptList.add(makeReceiptForUser(userFromDb,resultSet));
                    }
                }

                } catch (SQLException throwables) {
                    log.error("Cannot get receipts for user "+user.getId(),throwables);
                }finally {
                    if (connection!=null){
                        closeResources(connection);
                    }
                    if (preparedStatement!=null){
                        closeResources(preparedStatement);
                    }
                }
                return receiptList;
            }
    public static Set<Receipt> getReceiptFromUser (User user, String statusName) throws InsertUserException {
        Set<Receipt> receiptList = new TreeSet<>();
        User userFromDb = UserDAO.getUser(user.getLogin());
        int userId = userFromDb.getId();
        Connection connection=null;
        PreparedStatement preparedStatement = null;
        try {
            connection=DBManager.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(GET_RECEIPT_FROM_USER2,PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1,userId);
            preparedStatement.setString(2, statusName);
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()){
                    receiptList.add(makeReceiptForUser(userFromDb,resultSet));
                }
            }

        } catch (SQLException throwables) {
            log.error("Cannot get receipts for user "+user.getId(),throwables);
        }finally {
            if (connection!=null){
                closeResources(connection);
            }
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
        }
        return receiptList;
    }




    /**
     * Method for making receipt for user
     * @param user - target user
     * @param resultSet - result set with data from database
     * @return Receipt for user
     * @throws SQLException
     */
    private static Receipt makeReceiptForUser(User user, ResultSet resultSet) throws SQLException {
                Receipt receipt=Receipt.getReceipt();
                    receipt.setId(resultSet.getInt("receipt.id"));
                    receipt.setStatus(resultSet.getInt("status_id"));
                    receipt.setStatusName(resultSet.getString("status.name"));
                    receipt.setCreateTime(resultSet.getTimestamp("receipt_has_food.create_time"));
                    receipt.setTotal(resultSet.getInt("total"));
                    receipt.setUser(user);
                receipt.setFoodList(getFoodForReceipt(receipt.getId()));
                return receipt;
            }

    /**
     * Method for mapping food from resultSet
     * @param resultSet contains data from database
     * @return food from resultSet
     * @throws SQLException
     */
            private static Food createFood(ResultSet resultSet) throws SQLException {
                Food food= Food.createFood();
                food.setCategory(resultSet.getInt("category_id"));
                food.setPrice(resultSet.getInt("receipt_has_food.price"));
                food.setName(resultSet.getString("food.name"));
                food.setAmount(resultSet.getInt("count"));
                food.setDescription(resultSet.getString("description"));
                return food;
            }

    /**
     * Method for mappint receipt from resultSet
     * @param resultSet resultSet with data
     * @return Receipt
     * @throws SQLException
     */
    private static Receipt mapReceipt(ResultSet resultSet) throws SQLException {
                Receipt receipt = Receipt.getReceipt();
                        receipt.setId(resultSet.getInt("receipt_id"));
                        receipt.setStatusName(resultSet.getString("status.name"));
                        receipt.setCreateTime(resultSet.getTimestamp("receipt_has_food.create_time"));
                        receipt.setUser(UserDAO.getUser(resultSet.getString("login")));
                        receipt.setStatus(resultSet.getInt("status_id"));
                        receipt.setTotal(resultSet.getInt("total"));
                        receipt.setFoodList(getFoodForReceipt(receipt.getId()));
                return receipt;
            }

    /**
     * Method for closing resources
     * @param closeable resources for closing
     */
    private static void closeResources(AutoCloseable closeable){
        try {
            closeable.close();
        } catch (Exception e) {
            log.error("Cannot close resource " + closeable.getClass().getName(),e);
        }
    }


}
