package com.example.restaurant.dao;

import com.example.restaurant.database.DBManager;
import com.example.restaurant.models.Food;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.sql.*;
import java.util.*;

/**
 * Food data access object.
 * Contains methods for interacting with database
 *
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
public class FoodDAO {
    /**
     * All query declared as constance.
     */
    private static final String LOAD_ALL_FOOD = "SELECT * FROM food WHERE category_id=?";
    private static final String GET_FOOD_COUNT = "SELECT count(id) from food WHERE category_id = (?)";
    private static final String SELECT_BY_NAME="SELECT * FROM food WHERE name = ?";
    private static final String GET_ALL_CATEGORY="SELECT * FROM category;";
    private static final String ADD_FOOD_TO_DB="INSERT INTO food (name, description, price, amount, picture, category_id) values ((?),(?),(?),(?),(?),(?));";
    private static final String DELETE_FOOD = " DELETE FROM food WHERE name = (?)";
    private static final String EDIT_FOOD = " UPDATE food set name =(?), description =(?), price=(?), amount=(?), picture =(?), category_id=(?)" +
            " where food.id = (?) ;";

    /**
     * initialize the logger for this class
     */
    private static final Logger log = LogManager.getLogger(FoodDAO.class);

    /**
     * Return the number of food in database by category, using for pagination.
     * @param categoryId using for get count of food by category.
     */
    public static int getFoodCount(int categoryId){
        int count=1;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
         connection=DBManager.getInstance().getConnection();
         preparedStatement=connection.prepareStatement(GET_FOOD_COUNT);
         preparedStatement.setInt(1,categoryId);
         resultSet=preparedStatement.executeQuery();
            if (resultSet.next()){
                count=resultSet.getInt(1);
            }
        } catch (SQLException throwables) {
            log.error("Cannot get foodCount",throwables);
        } finally {
            if (resultSet!=null){
                closeResources(resultSet);
            }
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
        return count;
    }

    /**
     * Edit(update) food from database.
     * @param food using for targeting the food.
     */
    public static void editFood(Food food){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection= DBManager.getInstance().getConnection();
            preparedStatement=connection.prepareStatement(EDIT_FOOD);
            preparedStatement.setString(1,food.getName());
            preparedStatement.setString(2,food.getDescription());
            preparedStatement.setInt(3,food.getPrice());
            preparedStatement.setInt(4,food.getAmount());
            preparedStatement.setString(5,food.getPicture());
            preparedStatement.setInt(6,food.getCategory());
            preparedStatement.setInt(7,food.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            log.error("Cannot edit food",throwables);
        } finally {
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
    }

    /**
     * Method for new food to database
     * @param food using for pass food from manager to database.
     */
    public static void addFoodToDb(Food food){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection= DBManager.getInstance().getConnection();
            preparedStatement=connection.prepareStatement(ADD_FOOD_TO_DB);
            preparedStatement.setString(1,food.getName());
            preparedStatement.setString(2,food.getDescription());
            preparedStatement.setInt(3,food.getPrice());
            preparedStatement.setInt(4,food.getAmount());
            preparedStatement.setString(5,food.getPicture());
            preparedStatement.setInt(6,food.getCategory());
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            log.error("Cannot add food to database",throwables);
        } finally {
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
    }

    /**
     * Method for delete food from database.
     * @param food using for find the right one food in database.
     */
    public static void deleteFood(Food food){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection= DBManager.getInstance().getConnection();
            preparedStatement=connection.prepareStatement(DELETE_FOOD);
            preparedStatement.setString(1,food.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            log.error("Cannot delete food",throwables);
        } finally {
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
    }

    /**
     * Method for getting List of food from database for manager.
     * @param foodCategoryId using for get list of food by category.
     */
    public static List<Food> getAllFood(int foodCategoryId){
        List<Food> foodList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection= DBManager.getInstance().getConnection();
            preparedStatement=connection.prepareStatement(LOAD_ALL_FOOD);
            preparedStatement.setInt(1,foodCategoryId);
            resultSet=preparedStatement.executeQuery();
            while (resultSet.next()){
                foodList.add(createFoodFromDB(resultSet));
            }
        } catch (SQLException throwables) {
            log.error("Cannot get List of all food",throwables);
        } finally {
            if (resultSet!=null){
                closeResources(resultSet);
            }
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
        return foodList;
    }
    /**
     * Method for getting all food for user, with sorting, category and pagination.
     * @param foodCategoryId using for selection by category.
     * @param orderBy using for ordering food.
     * @param startLimit using for pagination.
     * @param endLimit using for pagination. Represent amount of food per page.
     */
    public static List<Food> getAllFood(int foodCategoryId, String orderBy, int startLimit, int endLimit){
        String LOAD_ALL_FOOD = "SELECT * FROM food WHERE amount>0 AND category_id = ? ORDER BY " + validateData(orderBy) +  " LIMIT ?,?;";
        List<Food> foodList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection= DBManager.getInstance().getConnection();
            preparedStatement=connection.prepareStatement(LOAD_ALL_FOOD);
            preparedStatement.setInt(1,foodCategoryId);
            preparedStatement.setInt(2,startLimit);
            preparedStatement.setInt(3,endLimit);
            resultSet=preparedStatement.executeQuery();
            while (resultSet.next()){
                foodList.add(createFoodFromDB(resultSet));
            }
        } catch (SQLException throwables) {
            log.error("Cannot get food for user menu",throwables);
        } finally {
            if (resultSet!=null){
                closeResources(resultSet);
            }
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
        return foodList;
    }

    /**
     * Check data for safety concat string in query.
     * @param data checked for safety
     */
    private static String validateData(String data){
        List<String> sortingList = new ArrayList<>();
        Collections.addAll(sortingList,"name","price");
        if (sortingList.contains(data)){
            return data;
        }else{
            return "id";
        }
    }

    /**
     * Method for dynamically getting all food`s category from database.
     */
    public static Map<Integer,String> getAllCategory(){
        Map<Integer,String> foodCategory = new HashMap<>();
        Connection connection=null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
          connection=DBManager.getInstance().getConnection();
          statement=connection.createStatement();
          resultSet=statement.executeQuery(GET_ALL_CATEGORY);
          while (resultSet.next()){
              foodCategory.put(resultSet.getInt("id"),resultSet.getString("name"));
          }

        } catch (SQLException throwables) {
            log.error("Cannot get all category for food",throwables);
        } finally {
            if (resultSet!=null){
                closeResources(resultSet);
            }
            if (statement!=null){
                closeResources(statement);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
        return foodCategory;
    }

    /**
     * Method for getting food by name, using for data update.
     */
    public static Food getFoodByName(String name){
        Food food = Food.createFood();
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        ResultSet resultSet = null;
        try {
            connection = DBManager.getInstance().getConnection();
            preparedStatement=connection.prepareStatement(SELECT_BY_NAME);
            preparedStatement.setString(1,name);
            resultSet=preparedStatement.executeQuery();
            if (resultSet.next()){
                food.setId(resultSet.getInt("id"));
                food.setName(resultSet.getString("name"));
                food.setDescription(resultSet.getString("description"));
                food.setFoodInBDamount(resultSet.getInt("amount"));
                food.setPrice(resultSet.getInt("price"));
                food.setPicture(resultSet.getString("picture"));
                food.setCategory(resultSet.getInt("category_id"));
            }else{
                log.info("Missing food with " + name);
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }finally {
            if (resultSet!=null){
                closeResources(resultSet);
            }
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
        return food;
    }

    /**
     * Method for creating food using resultSet.
     * @param resultSet contains info from database.
     */
    public static Food createFoodFromDB(ResultSet resultSet) throws SQLException {
        Food food = Food.createFood();
        food.setId(resultSet.getInt("id"));
        food.setName(resultSet.getString("name"));
        food.setDescription(resultSet.getString("description"));
        food.setAmount(resultSet.getInt("amount"));
        food.setCategory(resultSet.getInt("category_id"));
        food.setPicture(resultSet.getString("picture"));
        food.setPrice(resultSet.getInt("price"));
        return food;
    }
    private static void closeResources(AutoCloseable closeable){
        try {
            closeable.close();
        } catch (Exception e) {
            log.error("Cannot close resource", e);
        }
    }


}
