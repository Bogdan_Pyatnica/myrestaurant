package com.example.restaurant.dao;
import at.favre.lib.crypto.bcrypt.BCrypt;
import com.example.restaurant.database.DBManager;

import com.example.restaurant.exceptions.InsertUserException;
import com.example.restaurant.models.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * User data access object.
 * Contains methods for interacting with database
 *
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
public class UserDAO {
    public static final String SELECT_ALL_FROM_USER = "SELECT * FROM user";
    public static final String SELECT_BY_LOGIN = "SELECT * FROM user WHERE login = ?;";
    public static final String SAVE_USER = "INSERT INTO user (login, email, password, user_name, role_id) VALUES (?,?,?,?,?);";
    public static final String REMOVE_USER = "DELETE FROM USER WHERE login=?;";

    /**
     * Logger for this class
     */
    private static final Logger log = LogManager.getLogger(UserDAO.class);

    /**
     * Method for deleting user by login
     * @param login - login of target user
     */
    public static void deleteUserByLogin(String login){
        Connection connection = null;
        PreparedStatement preparedStatement =null;
        try{
            connection = DBManager.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(REMOVE_USER);
            preparedStatement.setString(1,login);
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            log.error("Cannot remove user with login"+login,throwables);
        }finally{
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
    }

    /**
     * Method for inserting user to Database
     * @param user - object of user to Insert
     */
    public static void insertUser(User user){
        Connection connection = null;
        PreparedStatement preparedStatement =null;
        try{
           connection = DBManager.getInstance().getConnection();
           preparedStatement = connection.prepareStatement(SAVE_USER,Statement.RETURN_GENERATED_KEYS);
           preparedStatement.setString(1,user.getLogin());
           preparedStatement.setString(2,user.getEmail());
           preparedStatement.setString(3,user.getPassword());
           preparedStatement.setString(4,user.getName());
           preparedStatement.setInt(5,user.getRole());
           preparedStatement.executeUpdate();
            try(ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet.next()){
                    user.setId(resultSet.getInt(1));
                }
            }
        } catch (SQLException throwables) {
            log.error("Cannot insert user "+user.getLogin(),throwables);
        }finally{
           if (preparedStatement!=null){
               closeResources(preparedStatement);
           }
           if (connection!=null){
               closeResources(connection);
           }
        }
    }

    /**
     * Method for mapping user from resultSet
     * @param resultSet - contains data for mapping
     * @return user from database
     * @throws SQLException
     */
    public static User createUserFromBD(ResultSet resultSet) throws SQLException {
        User user = User.createUser();
        user.setId(resultSet.getInt("id"));
        user.setLogin(resultSet.getString("login"));
        user.setPassword(resultSet.getString("password"));
        user.setName(resultSet.getString("user_name"));
        user.setEmail(resultSet.getString("email"));
        user.setRole(resultSet.getInt("role_id"));
        return user;
    }

    /**
     * Method for displaying all user from database
     * @return list of all users
     */
    public static List<User> getAllUsers(){
        List<User> userList = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection=DBManager.getInstance().getConnection();
            statement=connection.createStatement();
            resultSet=statement.executeQuery(SELECT_ALL_FROM_USER);
            while (resultSet.next()){
                userList.add(createUserFromBD(resultSet));
            }
        } catch (SQLException throwables) {
            log.error("Cannot get list of users ",throwables);

        } finally {
            if (resultSet!=null){
                closeResources(resultSet);
            }
            if (statement!=null){
                closeResources(statement);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
        return userList;
    }

    /**
     * Method for getting user by login
     * @param login - target user login
     * @return user from database
     */
    public static User getUser(String login){
        User user = User.createUser();
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        ResultSet resultSet = null;
        try {
           connection = DBManager.getInstance().getConnection();
           preparedStatement=connection.prepareStatement(SELECT_BY_LOGIN);
           preparedStatement.setString(1,login);
           resultSet=preparedStatement.executeQuery();
           if (resultSet.next()){
               user.setId(resultSet.getInt("id"));
               user.setLogin(resultSet.getString("login"));
               user.setEmail(resultSet.getString("email"));
               user.setPassword(resultSet.getString("password"));
               user.setName(resultSet.getString("user_name"));
               user.setRole(resultSet.getInt("role_id"));
           }else{
              log.info("User does not exist " + login);
           }
        } catch (SQLException throwable) {
            log.error("Cannot get user by login "+ login,throwable);
        }finally {
            if (resultSet!=null){
                closeResources(resultSet);
            }
            if (preparedStatement!=null){
                closeResources(preparedStatement);
            }
            if (connection!=null){
                closeResources(connection);
            }
        }
        return user;
    }

    /**
     * Method for sing In. Check if that user in database and checking password
     * @param login login of user
     * @param password password
     * @return user with null fields if that user not in database or that user, if it in database
     * @throws InsertUserException - throw if user not in database
     */
    public static User userSingIn(String login, String password) throws InsertUserException {
        if (login!=null && password!=null){
            User userCheck = getUser(login);
            //mock password if user not in database
            if (userCheck.getPassword()==null){
                userCheck.setPassword("1");
            }
            BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), userCheck.getPassword());
            if (userCheck.getPassword()!=null && result.verified){
                    return userCheck;
            }else{
                log.error("Error, User not in database! "+login);
                throw new InsertUserException("Cannot insert user!");
            }
        }
        return User.createUser();
    }

    /**
     * Method for validating user
     * @param user user for validate
     * @return true if that user already in database otherwise @return false
     */
    public static boolean validateUser(User user) {
    User userCheck = getUser(user.getLogin());
    //mock password if user not in database
    if (userCheck.getPassword()==null){
        userCheck.setPassword("1");
    }
    BCrypt.Result result = BCrypt.verifyer().verify(user.getPassword().toCharArray(), userCheck.getPassword());
    if (userCheck.getLogin() == null ){
        return false;
    }else if (userCheck.getLogin().equals(user.getLogin()) && result.verified){
        return true;
    }else {
        return false;
    }
}

    /**
     * Method dor closing resources
     * @param closeable - resource fro closing
     */
    private static void closeResources(AutoCloseable closeable){
        try {
            closeable.close();
        } catch (Exception e) {
            log.error("Cannot close resource "+closeable.getClass().getName(),e);
        }
    }




}
