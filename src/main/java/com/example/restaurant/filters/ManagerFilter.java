package com.example.restaurant.filters;
import com.example.restaurant.models.User;
import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;


import javax.servlet.http.HttpSession;
import java.io.IOException;
/**
 * Filter for manager resources
 * do not pass user to manager page
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
@WebFilter(urlPatterns = {"/UsersList","/manager_pages/*","/EditStatusServlet","/AllReceipt"},dispatcherTypes = DispatcherType.FORWARD)
public class ManagerFilter implements Filter {
    FilterConfig filterConfig;
    public void init(FilterConfig config) throws ServletException {
        filterConfig=config;
    }
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest requestHTTP = (HttpServletRequest) request;
        HttpSession session = requestHTTP.getSession(true);
        if (!session.isNew() && session.getAttribute("user")!=null){
            User user = (User) session.getAttribute("user");
            if (user.getRole()==User.ROLE_MANAGER){
                chain.doFilter(request,response);
            }else {
                session.invalidate();
            request.getRequestDispatcher("/error_pages/permission_error.jsp").forward(request, response);
            }
        }

    }
}
