package com.example.restaurant.filters;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Filter for validate registration data from html forms
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
@WebFilter(urlPatterns = {"/SaveServlet","/SingIn"})
public class RegisterValidationFilter implements Filter {
    private static final Logger log = LogManager.getLogger(RegisterValidationFilter.class);
    FilterConfig filterConfig;
    private static String emailRegex = "\\S+@\\S+\\.\\S+";
    private static String loginRegex = "^(?=.{0,10}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._А-яЄЇєїґЁё]+(?<![_.])$";
    private static String passwordRegex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{4,10}$";

    public void init(FilterConfig config) throws ServletException {
        filterConfig = config;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletResponse responseSERV = (HttpServletResponse) response;
        log.info("DO FILTER FOR REG VALIDATION");

        String login = request.getParameter("login");
        String password = request.getParameter("password");

        if (request.getParameter("register_form") != null) {
            String email = request.getParameter("email");
            if (login == null || "".equals(login) || password == null || "".equals(password) || email == null || "".equals(email)) {
                responseSERV.sendRedirect("error_pages/registration_error.jsp");
            }else if(login.matches(loginRegex) && email.matches(emailRegex) && password.matches(passwordRegex)){
                chain.doFilter(request,response);
            }else{
                responseSERV.sendRedirect("error_pages/registration_error.jsp");
            }
        }
        if (request.getParameter("sign_in_form") != null) {
                System.out.println(login + " " + password);
                System.out.println("Hello sign in!");
                if (login == null || "".equals(login) || password == null || "".equals(password)) {
                    responseSERV.sendRedirect("error_pages/registration_error.jsp");
                } else if (login.matches(loginRegex) && password.matches(passwordRegex)) {
                    System.out.println("matched!");
                    chain.doFilter(request, response);
                } else {
                    responseSERV.sendRedirect("error_pages/registration_error.jsp");
                }
        }

    }
}

