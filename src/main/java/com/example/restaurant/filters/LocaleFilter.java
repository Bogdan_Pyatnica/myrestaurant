package com.example.restaurant.filters;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
/**
 * Filter for passing locale through all pages
 * set session locale attribute if it null
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
@WebFilter(urlPatterns = {"/*"})
public class LocaleFilter implements Filter {
    FilterConfig filterConfig;
    public void init(FilterConfig config) throws ServletException {
        filterConfig=config;
    }
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        if (httpServletRequest.getParameter("sessionLocale") !=null){
            httpServletRequest.getSession().setAttribute("lang",httpServletRequest.getParameter("sessionLocale"));
        }
        chain.doFilter(request, response);
    }
}
