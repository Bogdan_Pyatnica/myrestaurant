package com.example.restaurant.models;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Receipt implements Comparable<Receipt> {
    public static final int STATUS_ORDERED=1;
    public static final int STATUS_IS_PREPARING=2;
    public static final int STATUS_READY=3;
    public static final int STATUS_DELIVERY=4;
    public static final int STATUS_ACCEPTED=5;
    public static final int STATUS_PRE_CANCEL=5;
    public static final int STATUS_CANCELED=6;

    private int id;
    private Timestamp createTime;
    private User user;
    private List<Food> foodList=new ArrayList<>();
    private int status=STATUS_ORDERED;
    private String statusName;
    private int total;
    private Receipt(){ }
    public static Receipt getReceipt(){
        return new Receipt();
    }
    public void addFood(Food food){
        foodList.add(food);
    }
    public void removeFood(Food food){
        foodList.remove(food);
    }
    public void setUser(User user) {
        this.user = user;
    }
    public User getUser() {
        return user;
    }
    public List<Food> getFoodList() {
        return foodList;
    }
    public void setStatus(int status) {
        this.status = status;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
    public int getStatus() {
        return status;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setFoodList(List<Food> foodList) {
        this.foodList = foodList;
    }

    @Override
    public String toString() {
        return "id: "+id+" User "+user.getId()+" "+user.getLogin() + " email " + user.getEmail()+" "+foodList;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Receipt receipt = (Receipt) o;
        return id == receipt.id && createTime.equals(receipt.createTime) && user.equals(receipt.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, createTime, user);
    }

    @Override
    public int compareTo(Receipt o) {
        return o.createTime.compareTo(createTime);
    }
}
