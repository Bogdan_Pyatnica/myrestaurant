package com.example.restaurant.models;

import java.util.Objects;

public class Food {
    private int id;
    private String name;
    private String description;
    private int price;
    private int amount = 1;
    private int foodInBDamount;
    private String picture;
    private int category;

    private Food(){}

    public static Food createFood(){
        return new Food();
    }

    public int getFoodInBDamount() {
        return foodInBDamount;
    }

    public void setFoodInBDamount(int foodInBDamount) {
        this.foodInBDamount = foodInBDamount;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Food food = (Food) o;
        return price == food.price && name.equals(food.name);
    }

    public int getPrice() {
        return price;
    }

    public int getAmount() {
        return amount;
    }

    public int increaseAmount(){
        return amount++;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price);
    }

    @Override
    public String toString() {
        return "Food " + name + " desription: " + description + " price: " + price + " category "+category;
    }
}
