package com.example.restaurant.exceptions;

public class MakeOrderException extends Exception{
    public MakeOrderException(String mess){
        super(mess);
    }
}
