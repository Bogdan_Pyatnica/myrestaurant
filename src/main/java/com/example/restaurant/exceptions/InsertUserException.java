package com.example.restaurant.exceptions;

public class InsertUserException extends Exception{
    public InsertUserException(String mess){
        super(mess);
    }
}
