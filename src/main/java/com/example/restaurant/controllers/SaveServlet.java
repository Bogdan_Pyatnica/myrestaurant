package com.example.restaurant.controllers;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.example.restaurant.dao.UserDAO;
import com.example.restaurant.models.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/**
 * Servlet for registration the user,
 * also check if that user already register and redirect him to signIn form.
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
@WebServlet("/SaveServlet")
public class SaveServlet extends HttpServlet {
    private static final Logger log = LogManager.getLogger(SaveServlet.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        String hashPass = BCrypt.withDefaults().hashToString(12,request.getParameter("password").toCharArray());
        if (user==null){
            user=User.createUser();
            user.setEmail(request.getParameter("email"));
            user.setName(request.getParameter("user_name"));
            user.setLogin(request.getParameter("login"));
            user.setPassword(hashPass);
            session.setAttribute("user",user);
        }
        if (UserDAO.validateUser(user)){
                session.invalidate();
                log.info("massage from saveUserServlet, user" + user.getLogin()+" already in database, redirect to sign in... ");
                response.sendRedirect("user_pages/sing_in.jsp");
        }else{
                UserDAO.insertUser(user);
                log.info("massage from saveUserServlet, user" + user.getLogin()+" has been saved");
                response.sendRedirect("user_pages/greetings.jsp");
        }
    }
}

