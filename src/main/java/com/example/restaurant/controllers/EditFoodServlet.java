package com.example.restaurant.controllers;

import com.example.restaurant.dao.FoodDAO;
import com.example.restaurant.models.Food;


import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Servlet for edit food view
 *  Check request parameter and do logic according to it
 *  doPost for various post form from view and redirecting where needed.
 *
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */

@WebServlet("/EditFoodServlet")
public class EditFoodServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Map<Integer,String> foodCategoryMap = FoodDAO.getAllCategory();
        List<Food> foodArrayEdit;
        if (session.getAttribute("user")==null){
            request.getRequestDispatcher("starting_page.jsp").forward(request,response);
        }
        if (request.getParameter("foodCategoryId")!=null){
            foodArrayEdit=FoodDAO.getAllFood(Integer.parseInt(request.getParameter("foodCategoryId")));
        }else{
            foodArrayEdit=FoodDAO.getAllFood(1);
        }
        request.setAttribute("foodCategoryMap", foodCategoryMap);
        request.setAttribute("foodArrayEdit",foodArrayEdit);
        request.getRequestDispatcher("manager_pages/edit_food.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<Integer,String> foodCategoryMap = FoodDAO.getAllCategory();
        if (request.getParameter("add_food_form") != null) {
            Food foodFromRequest = mapFood(request);
            FoodDAO.addFoodToDb(foodFromRequest);
            response.sendRedirect("EditFoodServlet");
        }else if (request.getParameter("delete_food")!=null){
            int categoryId = Integer.parseInt(request.getParameter("foodCategoryId"));
            String deleteFoodName = request.getParameter("food_name");
            FoodDAO.deleteFood(FoodDAO.getFoodByName(deleteFoodName));
            response.sendRedirect("EditFoodServlet?foodCategoryId="+categoryId);
        }else if (request.getParameter("edit_food")!=null){
            String editFoodName = request.getParameter("food_name");
            Food foodEdit = FoodDAO.getFoodByName(editFoodName);
            request.setAttribute("edit_food_obj",foodEdit);
            request.setAttribute("foodCategoryMap",foodCategoryMap);
            request.getRequestDispatcher("manager_pages/edit_old_food.jsp").forward(request,response);
        }else if(request.getParameter("edit_old_food_form")!=null){
            int foodId = Integer.parseInt(request.getParameter("food_edited_id"));
            Food foodEdited = mapFood(request);
            foodEdited.setId(foodId);
            FoodDAO.editFood(foodEdited);
            response.sendRedirect("EditFoodServlet");
        }else if (request.getParameter("add_new_food")!=null){
            request.setAttribute("foodCategoryMap", foodCategoryMap);
            request.getRequestDispatcher("manager_pages/add_new_food.jsp").forward(request,response);
        }
    }

    /**
     * Helper to map food edit request
     * @param request request from form
     * @return food from htmlForm
     */
    private static Food mapFood(HttpServletRequest request){
        Food mapFood = Food.createFood();
        mapFood.setName(request.getParameter("food_name"));
        mapFood.setDescription(request.getParameter("food_description"));
        mapFood.setPrice(Integer.parseInt(request.getParameter("food_price")));
        mapFood.setAmount(Integer.parseInt(request.getParameter("food_amount")));
        mapFood.setPicture(request.getParameter("food_picture"));
        mapFood.setCategory(Integer.parseInt(request.getParameter("category_id")));
        System.out.println(mapFood);
        return mapFood;
    }
}
