package com.example.restaurant.controllers;

import com.example.restaurant.dao.ReceiptDAO;
import com.example.restaurant.models.Receipt;


import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
/**
 * Servlet for edit status for user order
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */

@WebServlet("/EditStatusServlet")
public class EditStatusServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("receipt_status_id")!=null && request.getParameter("receipt_id")!=null){
            int receiptId = Integer.parseInt(request.getParameter("receipt_id"));
            int statusId = Integer.parseInt(request.getParameter("receipt_status_id"));
            if (statusId<Receipt.STATUS_ACCEPTED){
                ReceiptDAO.updateReceiptStatus(receiptId,statusId);
            }
        }
        if (request.getParameter("receipt_id_canceled")!=null){
            int receiptStatus = Integer.parseInt(request.getParameter("receipt_status"));
            if (receiptStatus<Receipt.STATUS_ACCEPTED){
                ReceiptDAO.cancelReceipt(Integer.parseInt(request.getParameter("receipt_id_canceled")));
            }
        }
        request.getRequestDispatcher("/AllReceipt").forward(request,response);
    }
}
