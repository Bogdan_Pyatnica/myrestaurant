package com.example.restaurant.controllers;


import com.example.restaurant.dao.ReceiptDAO;
import com.example.restaurant.exceptions.InsertUserException;
import com.example.restaurant.models.Food;
import com.example.restaurant.models.Receipt;
import com.example.restaurant.models.User;
import com.example.restaurant.utils.MailSender;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
 * Servlet user orders info
 * Load all receipt for user, and statuses for receipt.
 * User can cancel or pay, if receipt has correct status for that
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
@WebServlet("/UserOrderServlet")
public class UserOrderServlet extends HttpServlet {
    private static final Logger log = LogManager.getLogger(UserOrderServlet.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Map<Integer,String> statusMap = ReceiptDAO.getAllReceiptStatus();
        request.setAttribute("statusMap",statusMap);
        Set<Receipt> receiptList;
        if (session.getAttribute("user")==null){
            request.getRequestDispatcher("starting_page.jsp").forward(request,response);
        }
        try{
            if(request.getParameter("statusName")!=null) {
                receiptList = ReceiptDAO.getReceiptFromUser((User) session.getAttribute("user"),request.getParameter("statusName"));
            }else{
                receiptList = ReceiptDAO.getReceiptFromUser((User) session.getAttribute("user"),"ordered");
            }
            request.setAttribute("receiptList",receiptList);
            request.getRequestDispatcher("user_pages/my_order.jsp").forward(request,response);
        } catch (InsertUserException e) {
            log.error("Cannot get user ",e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session.getAttribute("user")==null){
            request.getRequestDispatcher("starting_page.jsp").forward(request,response);
        }
        User user = (User) session.getAttribute("user");
        if (request.getParameter("cancel_receipt_form")!=null){
            int receiptId = Integer.parseInt(request.getParameter("receipt_id"));
            int currentStatus = Integer.parseInt(request.getParameter("receipt_status"));
            if (currentStatus<Receipt.STATUS_ACCEPTED){
                ReceiptDAO.cancelReceipt(receiptId);
            }
            response.sendRedirect("UserOrderServlet");
        } else if (request.getParameter("load_food_receipt_id")!=null){
            int receiptId = Integer.parseInt(request.getParameter("load_food_receipt_id"));
            List<Food> foodInReceipt = ReceiptDAO.getFoodForReceipt(receiptId);
            request.setAttribute("food_for_receipt",foodInReceipt);
            request.getRequestDispatcher("user_pages/user_receipt_info.jsp").forward(request,response);
        } else if (request.getParameter("pay_form")!=null){
            int currentStatus = Integer.parseInt(request.getParameter("receipt_status"));
            int payReceiptId = Integer.parseInt(request.getParameter("receipt_id"));
            if (currentStatus==Receipt.STATUS_DELIVERY){
                ReceiptDAO.updateReceiptStatus(payReceiptId,currentStatus);
                List<Food> infoFoods = ReceiptDAO.getFoodForReceipt(payReceiptId);
                MailSender.sendReceipt(user,ReceiptDAO.getReceiptById(payReceiptId),infoFoods);
            }
            response.sendRedirect("UserOrderServlet");
        }
    }
}
