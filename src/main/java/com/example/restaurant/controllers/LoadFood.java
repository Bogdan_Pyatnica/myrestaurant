package com.example.restaurant.controllers;

import com.example.restaurant.dao.FoodDAO;
import com.example.restaurant.models.Food;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet for Load food to user menu view
 * has default values for starting sorting/pagination/selection
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
@WebServlet("/LoadFood")
public class LoadFood extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session.getAttribute("user") == null) {
            request.getRequestDispatcher("starting_page.jsp").forward(request, response);
        }
       // contains the food from selection
        List<Food> foodArray;
        Map<Integer, String> foodCategory = FoodDAO.getAllCategory();
        //get map of sorting parameters
        Map<String, String> sortBy = new HashMap<>();
        sortBy.put("Price", "price");
        sortBy.put("Name", "name");
        //set default attribute
        request.setAttribute("foodSorting", sortBy);
        request.setAttribute("foodCategory", foodCategory);

        //Food per/page for pagination
        int foodPerPage = 3;
        //just initialize it, count of food by category
        int foodsByCategory=1;
        if (request.getParameter("foodCategoryId")!=null){
            foodsByCategory = FoodDAO.getFoodCount(Integer.parseInt(request.getParameter("foodCategoryId")));
        }
        //number of pages
        int numOfPages = (int) Math.ceil((double) foodsByCategory/foodPerPage);
        request.setAttribute("numOfPages", numOfPages);
        request.setAttribute("foodPerPage",foodPerPage);

        if (request.getParameter("foodCategoryId")==null && request.getParameter("foodSort")==null && request.getParameter("currentPage")==null){
            request.setAttribute("foodSort","id");
            request.setAttribute("foodCategoryId",3);
            request.setAttribute("currentPage", 1);
            foodArray = FoodDAO.getAllFood(3,"id",0,foodPerPage);
        }else if(request.getParameter("foodCategoryId")!=null && request.getParameter("foodSort")!=null && request.getParameter("currentPage")!=null ){

            request.setAttribute("currentPage",request.getParameter("currentPage"));
            request.setAttribute("foodSort",request.getParameter("foodSort"));
            request.setAttribute("foodCategoryId",request.getParameter("foodCategoryId"));

            int currentPage = Integer.parseInt(request.getParameter("currentPage"));
            String foodSort = request.getParameter("foodSort");
            int foodCategoryId = Integer.parseInt(request.getParameter("foodCategoryId"));

            foodArray = FoodDAO.getAllFood(foodCategoryId,foodSort,(currentPage-1)*foodPerPage,foodPerPage);

        }else {
            foodArray = FoodDAO.getAllFood(1,"id",0,foodPerPage);
        }
        request.setAttribute("foodArray",foodArray);
        request.getRequestDispatcher("user_pages/food_list.jsp").forward(request,response);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}


