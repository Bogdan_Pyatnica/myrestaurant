package com.example.restaurant.controllers;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
/**
 * Servlet for signingOut
 * simply invalidate user session and forwarding user to starting page
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
@WebServlet("/SingOut")
public class SingOut extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("user")!=null){
               session.invalidate();
               request.getRequestDispatcher("/starting_page.jsp").forward(request,response);
        }else{
            request.getRequestDispatcher("/starting_page.jsp").forward(request,response);
        }
    }
}
