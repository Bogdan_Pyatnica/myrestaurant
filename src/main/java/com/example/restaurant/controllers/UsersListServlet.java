package com.example.restaurant.controllers;

import com.example.restaurant.dao.UserDAO;
import com.example.restaurant.models.User;


import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;
/**
 * Servlet for Load all user list to manager page
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
@WebServlet("/UsersList")
public class UsersListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<User> userList = UserDAO.getAllUsers();
        HttpSession session = request.getSession(false);

        if (session.getAttribute("user")==null){
            request.getRequestDispatcher("starting_page.jsp").forward(request,response);
        }else {
            request.setAttribute("userArray",userList);
            request.getRequestDispatcher("manager_pages/users_list.jsp").forward(request,response);
        }
    }
}
