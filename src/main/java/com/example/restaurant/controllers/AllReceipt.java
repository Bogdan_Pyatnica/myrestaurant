package com.example.restaurant.controllers;


import com.example.restaurant.dao.ReceiptDAO;
import com.example.restaurant.models.Receipt;


import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
/**
 * Servlet for getting list of all receipt for manager
 * And get all statuses for passing it to view
 * doPost for status logic
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
@WebServlet("/AllReceipt")
public class AllReceipt extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Map<Integer,String> statusMap = ReceiptDAO.getAllReceiptStatus();
        request.setAttribute("statusMap",statusMap);
        Set<Receipt> receiptSet;
        if (session.getAttribute("user")==null){
            request.getRequestDispatcher("starting_page.jsp").forward(request,response);
        }
        if(request.getParameter("statusName")!=null) {
            receiptSet = ReceiptDAO.getAllReceipt(request.getParameter("statusName"));
        }else{
            receiptSet = ReceiptDAO.getAllReceipt("ordered");
        }
        request.setAttribute("receiptSet",receiptSet);
        request.getRequestDispatcher("manager_pages/receipt_list.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
