package com.example.restaurant.controllers;

import com.example.restaurant.dao.ReceiptDAO;
import com.example.restaurant.models.Food;
import com.example.restaurant.models.User;
import com.example.restaurant.utils.MailSender;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;


import java.util.List;
/**
 * Servlet for submitting user order
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
@WebServlet("/SubmitOrderServlet")
public class SubmitOrderServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        List<Food> foodList ;
        User user = (User) session.getAttribute("user");
        if (session.getAttribute("user")==null){
            request.getRequestDispatcher("starting_page.jsp").forward(request,response);
        }
        if (session.getAttribute("food_ordered")==null){
            request.getRequestDispatcher("/LoadFood").forward(request,response);
        }else{
            foodList = (List<Food>) session.getAttribute("food_ordered");
            Food[] foods = foodList.toArray(new Food[foodList.size()]);
            ReceiptDAO.makeReceipt(user,foods);
            session.removeAttribute("food_ordered");
            response.sendRedirect("user_pages/user_profile.jsp");
        }

    }
}
