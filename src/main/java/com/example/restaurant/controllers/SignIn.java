package com.example.restaurant.controllers;

import com.example.restaurant.dao.FoodDAO;
import com.example.restaurant.dao.UserDAO;
import com.example.restaurant.exceptions.InsertUserException;
import com.example.restaurant.models.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
/**
 * Servlet for SignIn
 * If user role manager - redirect to manager pages, if not - user pages.
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
@WebServlet("/SignIn")
public class SignIn extends HttpServlet {
    private static final Logger log = LogManager.getLogger(SignIn.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setMaxInactiveInterval(3*60);
        User user = (User) session.getAttribute("user");
        if (user==null){
            try {
                user=UserDAO.userSingIn(request.getParameter("login"),request.getParameter("password"));
                session.setAttribute("user",user);
            } catch (InsertUserException e) {
                log.error("Cannot insert user", e);
                response.sendRedirect("starting_page.jsp");
            }
        }
        if(user!=null && user.getRole()==User.ROLE_MANAGER){
            response.sendRedirect("manager_pages/manager_profile.jsp");
        }else if (user!=null && user.getRole()==User.ROLE_USER) {
            response.sendRedirect("user_pages/greetings_sing.jsp");
        }

    }
    //Using doPost while user fill signIn html form and press submit.
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
