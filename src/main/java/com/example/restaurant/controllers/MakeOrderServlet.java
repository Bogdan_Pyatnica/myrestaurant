package com.example.restaurant.controllers;
import com.example.restaurant.dao.FoodDAO;
import com.example.restaurant.models.Food;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * Servlet for making order
 * contains food for order in list, and passing in to session and back from different view.
 * removed from session when user make order or delete all food from cart
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
@WebServlet("/MakeOrderServlet")
public class MakeOrderServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        List<Food> foodList;
        int category=1;
        int currentPage= 1;
        if(request.getParameter("foodCategoryId")!=null){
            category= Integer.parseInt(request.getParameter("foodCategoryId"));
        }
        if (request.getParameter("currentPage")!=null){
            currentPage=Integer.parseInt(request.getParameter("currentPage"));
        }

        String sortBy=request.getParameter("foodSort");

        if (session.getAttribute("user")==null){
            request.getRequestDispatcher("starting_page.jsp").forward(request,response);
        }
        if (session.getAttribute("food_ordered")==null){
            String foodName = request.getParameter("food_name");
            Food foodFromDb = FoodDAO.getFoodByName(foodName);
            foodList=new ArrayList<>();
            if (foodFromDb.getFoodInBDamount()>=foodFromDb.getAmount()){
                foodList.add(FoodDAO.getFoodByName(foodName));
            }
            session.setAttribute("food_ordered",foodList);
            response.sendRedirect("LoadFood?foodCategoryId="+category+"&foodSort="+sortBy+"&currentPage="+currentPage);
        }else if (session.getAttribute("food_ordered")!=null && request.getParameter("food_name")!=null ){
                String foodName =  request.getParameter("food_name");
                foodList= (List<Food>) session.getAttribute("food_ordered");
                session.removeAttribute("food_ordered");
                Food foodCheck = FoodDAO.getFoodByName(foodName);

                if (foodList.contains(foodCheck)){
                    int index = foodList.indexOf(foodCheck);
                    int amount = foodList.get(index).getAmount();
                    if (foodCheck.getFoodInBDamount()>amount){
                        foodCheck.setAmount(++amount);
                        foodList.set(index,foodCheck);
                    }
                }else{
                    foodList.add(FoodDAO.getFoodByName(foodName));
                }
                session.setAttribute("food_ordered",foodList);
                response.sendRedirect("LoadFood?foodCategoryId="+category+"&foodSort="+sortBy+"&currentPage="+currentPage);
        }else if (request.getParameter("food_remove")!=null){
            String removeFood = request.getParameter("food_remove");
            foodList= (List<Food>) session.getAttribute("food_ordered");
            int indexRemove = foodList.indexOf(FoodDAO.getFoodByName(removeFood));
            Food foodRemoveCheck = foodList.get(indexRemove);
            session.removeAttribute("food_ordered");
            if (foodRemoveCheck.getAmount()>=1){
                int index = foodList.indexOf(foodRemoveCheck);
                int amount = foodRemoveCheck.getAmount();
                foodRemoveCheck.setAmount(amount-1);
                if (foodRemoveCheck.getAmount()<1){
                    foodList.remove(FoodDAO.getFoodByName(removeFood));
                }else {
                    foodList.set(index,foodRemoveCheck);
                }
            }
            session.setAttribute("food_ordered",foodList);
            response.sendRedirect("user_pages/user_cart.jsp");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }
}
