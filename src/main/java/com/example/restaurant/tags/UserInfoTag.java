package com.example.restaurant.tags;


import com.example.restaurant.models.User;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
/**
 * My Own simple tag.
 * Just trying how to do it.
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
public class UserInfoTag extends SimpleTagSupport {
    User user;
    public UserInfoTag(){}
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void doTag() throws JspException, IOException {
        try {
            if (user!=null){
                getJspContext().getOut().append(user.getName())
                        .append(" ")
                        .append(user.getLogin())
                        .append(" ")
                        .append(user.getEmail())
                        .close();
            }else{
                getJspContext().getOut().write("Empty user!");
            }

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}

