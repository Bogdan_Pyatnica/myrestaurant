package com.example.restaurant.utils;

import com.example.restaurant.models.Food;
import com.example.restaurant.models.Receipt;
import com.example.restaurant.models.User;
import com.itextpdf.text.DocumentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Properties;
/**
 * Class for sending user an email when user press 'pay' button.
 * Forming simple html form for email message
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
public class MailSender {
    private static final String USER_NAME ="restourantnotify@gmail.com";
    private static final String APP_PASSWORD = "TestTest111";
    private static final Logger log = LogManager.getLogger(MailSender.class);

    private MailSender(){}

    /**
     * Method for forming an email and send it to recipient
     * @param user - target user, should contain an email and login
     * @param userReceipt - target receipt for user
     * @param foodList - list of food for target receipt
     */
    public static void sendReceipt(User user, Receipt userReceipt, List<Food> foodList){
        String sendTo = user.getEmail();
        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "465");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.socketFactory.port", "465");
        prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(USER_NAME, APP_PASSWORD);
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(sendTo)
            );
            //Forming an email
            message.setSubject("Restaurant, order for "+ user.getLogin());
            // we need byteArrayOutputStream for in-memory forming PDF.
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            //Write pdf to byteArrayOutputStream
            PDFWriter.writePDFEmailReport(byteArrayOutputStream,userReceipt,user, foodList);
            // get PFD bytes from byteArrayOutputStream
            byte[] bufferBytes = byteArrayOutputStream.toByteArray();
            DataSource dataSource = new ByteArrayDataSource(bufferBytes,"application/pdf");
            MimeBodyPart pdfBody = new MimeBodyPart();
            MimeBodyPart textBody = new MimeBodyPart();
            textBody.setContent(buildMassage(userReceipt),"text/html");
            pdfBody.setDataHandler(new DataHandler(dataSource));
            pdfBody.setFileName("order_info_"+userReceipt.getId()+".pdf");
            MimeMultipart mimeMultipart = new MimeMultipart();
            mimeMultipart.addBodyPart(pdfBody);
            mimeMultipart.addBodyPart(textBody);
            message.setContent(mimeMultipart);
            Transport.send(message);
            log.info("Message was send to " + sendTo);
        } catch (MessagingException | DocumentException e) {
            log.error("Error while sending massage to " + sendTo, e);
        }
    }

    private static String buildMassage(Receipt receipt){
        StringBuilder stringBuilder = new StringBuilder();
       String message =  stringBuilder.append("<h1>")
                .append("Order id: ").append(receipt.getId())
                .append("<br>")
                .append("Total price: ").append(receipt.getTotal())
                .append("<br>")
                .append("Status: payed.").append("</h1>").toString();
       return message;
    }

}
