package com.example.restaurant.utils;

import com.example.restaurant.models.Food;
import com.example.restaurant.models.Receipt;
import com.example.restaurant.models.User;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.OutputStream;
import java.util.List;
/**
 * Class for forming PDF files for user.
 * @author - Bohdan Piatnytsia
 * @version - 1.0
 */
public class PDFWriter {
    private PDFWriter(){}
    /**
     * Method for forming PDF for target user and target receipt info.
     * @param outputStream - stream that contains our formed PDF for in-memory use.
     * @param receipt - target receipt.
     * @param user - target user.
     * @param foodList - list of product in target receipt
     * @throws DocumentException
     */
    public static void writePDFEmailReport(OutputStream outputStream, Receipt receipt, User user, List<Food> foodList) throws DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, outputStream);
        document.open();
        document.addTitle("Receipt info for order id:"+receipt.getId());
        document.addSubject("Order info");
        Paragraph paragraph = new Paragraph();
        document.add(new Paragraph("Order info: "));
        document.add(new Chunk("User: " + user.getLogin() + ", order id: "+receipt.getId()));
        document.add(new Paragraph("Item(s) in order: "));
        com.itextpdf.text.List orderedList = new com.itextpdf.text.List(com.itextpdf.text.List.ORDERED);
        for (Food f: foodList){
            orderedList.add(new ListItem(f.getName() + ", amount:  "+f.getAmount()));
        }
        document.add(orderedList);
        document.add(new Paragraph("Total price: " + receipt.getTotal()));
        document.add(new Paragraph("Order status: payed"));
        document.add(paragraph);
        document.close();
    }
}
