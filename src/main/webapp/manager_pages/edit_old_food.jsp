<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 17.02.2021
  Time: 17:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/form_style.css">
    <title><fmt:message key="manager_pages_add_new_food.edit"/></title>
</head>
<body class="text-field">
<main class="form-signin">
    <div>
    <h1><fmt:message key="manager_pages_add_new_food.edit"/></h1><br>
    <form action="${pageContext.request.contextPath}/EditFoodServlet" method="post">
            <h2><fmt:message key="manager_pages_add_new_food.food"/></h2><br>
            <b><fmt:message key="manager_pages_add_new_food.name"/></b><br>
            <input type="text" name="food_name" value="${requestScope.edit_food_obj.name}"/><br>
            <b><fmt:message key="manager_pages_add_new_food.description"/></b><br>
            <input type="text" name="food_description" value="${requestScope.edit_food_obj.description}"/><br>
            <b><fmt:message key="manager_pages_add_new_food.price"/></b><br>
            <input type="text" name="food_price" value="${requestScope.edit_food_obj.price}"/><br>
            <b><fmt:message key="manager_pages_add_new_food.amount"/></b><br>
            <input type="text" name="food_amount" value="${requestScope.edit_food_obj.foodInBDamount}"/><br>
            <b><fmt:message key="manager_pages_add_new_food.category"/></b><br>
            <c:forEach items="${requestScope.foodCategoryMap}" var="category">
                <input type="radio" id="${category.key}" name="category_id" value="${category.key}" checked=${requestScope.edit_food_obj.category}><c:out value="${category.value}"/><br>
            </c:forEach>
            <b><fmt:message key="manager_pages_add_new_food.picture"/></b><br>
            <input type="text" name="food_picture" value="${requestScope.edit_food_obj.picture}">
            <input type="hidden" name="food_edited_id" value="${requestScope.edit_food_obj.id}">
            <input type="hidden" name="edit_old_food_form">
            <input type="submit" value="<fmt:message key="manager_pages_add_new_food.submit_change"/>"/><br>
        </div>
    </form>
    <a href="${pageContext.request.contextPath}/EditFoodServlet"><fmt:message key="manager_pages_users.back"/></a>
</main>
</body>
</html>
