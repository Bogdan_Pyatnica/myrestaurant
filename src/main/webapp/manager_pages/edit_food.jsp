<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 14.02.2021
  Time: 19:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <jsp:include page="/navigation/manager_navigation_bar.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/table_style.css">
    <title><fmt:message key="manager_pages_add_new_food.edit"/></title>
</head>
<body>
<h3>
    <p><fmt:message key="manager_pages_add_new_food.category"/></p>
    <div class="topnav">
        <c:forEach items="${requestScope.foodCategoryMap}" var="category">
            <a href="${pageContext.request.contextPath}/EditFoodServlet?foodCategoryId=${category.key}">${category.value}</a>
        </c:forEach>
    </div>
    <div>
        <form action="${pageContext.request.contextPath}/EditFoodServlet" method="post">
            <input type="hidden" name="add_new_food">
            <input type="hidden" name="foodCategoryMap" value="${requestScope.foodCategoryMap}">
            <input type="submit" value="<fmt:message key="manager_pages_add_new_food.submit_add_new"/>">
        </form>
        <table id="information" border="1" cellpadding="5" cellspacing="1" >
            <tr>
                <th>Id</th>
                <th><fmt:message key="manager_pages_add_new_food.name"/></th>
                <th><fmt:message key="manager_pages_add_new_food.amount"/></th>
                <th><fmt:message key="manager_pages_add_new_food.edit"/></th>
                <td><fmt:message key="manager_pages_add_new_food.submit_delete"/></td>
            </tr>
            <c:forEach var="foods" items="${requestScope.foodArrayEdit}">
                <tr>
                    <td>${foods.id}</td>
                    <td>${foods.name}</td>
                    <td>${foods.amount}</td>
                    <td>
                        <form action="${pageContext.request.contextPath}/EditFoodServlet" method="post">
                            <input type="hidden" name="food_name" value="${foods.name}">
                            <input type="hidden" name="edit_food">
                            <input type="hidden" name="foodCategoryId" value="${foods.category}">
                            <input type="submit" value="<fmt:message key="manager_pages_add_new_food.edit"/>">
                        </form>
                    </td>
                    <td>
                        <form onsubmit="return msg(this);" action="${pageContext.request.contextPath}/EditFoodServlet" method="post">
                            <input type="hidden" name="food_name" value="${foods.name}">
                            <input type="hidden" name="delete_food">
                            <input type="hidden" name="foodCategoryId" value="${foods.category}">
                            <input type="submit" value="<fmt:message key="manager_pages_add_new_food.submit_delete"/>">
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <script>
        function msg() {
            return confirm('<fmt:message key="manager_pages_add_new_food.confirm_delete_msg"/>');
        }
    </script>
</h3>
</body>
</html>
