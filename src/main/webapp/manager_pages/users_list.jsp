<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 26.01.2021
  Time: 21:55
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <title><fmt:message key="manager_pages_users.tittle"/></title>
    <jsp:include page="/navigation/manager_navigation_bar.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/table_style.css">
</head>
<body>
<div class="info">
<h1><fmt:message key="manager_pages_profile.info"/></h1>
<h3>
    <table id="information" border="1" cellpadding="5" cellspacing="1" >
        <tr>
            <th>ID</th>
            <th><fmt:message key="manager_pages_users.name"/></th>
            <th><fmt:message key="manager_pages_receipt_list.user_login"/></th>
            <th><fmt:message key="manager_pages_receipt_list.user_email"/></th>
        </tr>
        <c:forEach items="${requestScope.userArray}" var="users" >
            <tr>
                <td>${users.id}</td>
                <td>${users.name}</td>
                <td>${users.login}</td>
                <td>${users.email}</td>
            </tr>
        </c:forEach>
    </table>
</h3>
<a href="${pageContext.request.contextPath}/manager_pages/manager_profile.jsp"><fmt:message key="manager_pages_users.back"/></a><br>
</div>
</body>
</html>
