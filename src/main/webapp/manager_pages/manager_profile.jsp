<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 29.01.2021
  Time: 19:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="userinfo" uri="http://mycompany.com" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <jsp:include page="/navigation/manager_navigation_bar.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
    <title><fmt:message key="manager_pages_profile.tittle"/></title>
</head>
<body>
<div class="info">
    <h1><fmt:message key="manager_pages_profile.info"/></h1>
    <fmt:message key="user_pages_user_profile.hello_msg"/>
    <p><userinfo:userInfo user="${sessionScope.user}"/></p>
</div>
</body>
</html>
