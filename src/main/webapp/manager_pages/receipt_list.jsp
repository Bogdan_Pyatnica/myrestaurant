<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 03.02.2021
  Time: 22:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <jsp:include page="/navigation/manager_navigation_bar.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/table_style.css">

    <title><fmt:message key="manager_pages_receipt_list.tittle"/></title>
</head>
<body>
<div class="info">
    <h1><fmt:message key="manager_pages_profile.info"/></h1>
    <h2><fmt:message key="manager_pages_receipt_list.statuses"/></h2>
    <h3>
        <div class="topnav">
            <c:forEach items="${requestScope.statusMap}" var="status">
                <a href="${pageContext.request.contextPath}/AllReceipt?statusName=${status.value}">${status.value}</a>
            </c:forEach>
        </div>
    </h3>

    <h3>
        <table id="information" border="1" cellpadding="5" cellspacing="1" >
            <tr>
                <th><fmt:message key="manager_pages_receipt_list.receipt_id"/></th>
                <th><fmt:message key="manager_pages_receipt_list.user_login"/></th>
                <th><fmt:message key="manager_pages_receipt_list.user_email"/></th>
                <th><fmt:message key="manager_pages_receipt_list.receipt_create_time"/></th>
                <th><fmt:message key="manager_pages_receipt_list.receipt_total"/></th>
                <th><fmt:message key="manager_pages_receipt_list.receipt_status"/></th>
                <th><fmt:message key="manager_pages_receipt_list.receipt_status_edit"/></th>
                <th><fmt:message key="manager_pages_receipt_list.receipt_cancel"/></th>
            </tr>
            <c:forEach items="${requestScope.receiptSet}" var="receipt" >
                <tr>
                    <td>${receipt.id}</td>
                    <td>${receipt.user.login}</td>
                    <td>${receipt.user.email}</td>
                    <fmt:message key="manager_pages_receipt_list.receipt_date_pattern" var="datePattern"/>
                    <td><fmt:formatDate value="${receipt.createTime}" pattern="${datePattern}"/></td>
                    <td>
                                <fmt:message key="user_pages_food_list.currency_coorse" var="currency_coorse"/>
                                <fmt:message key="user_pages_food_list.price"/> <fmt:formatNumber value="${receipt.total / currency_coorse}" type="currency"/>
                    </td>
                    <td>${receipt.statusName}</td>
                    <td>
                        <c:if test="${receipt.status<5}">
                        <form action="${pageContext.request.contextPath}/EditStatusServlet" method="post">
                            <input type="hidden" name="receipt_id" value="${receipt.id}">
                            <input type="hidden" name="receipt_status_id" value="${receipt.status}">
                            <input type="hidden" name="statusName" value="${receipt.statusName}">
                            <input type="submit" value="<fmt:message key="manager_pages_receipt_list.receipt_submit_change"/>">
                        </form>
                        </c:if>
                    </td>
                    <td>
                        <c:if test="${receipt.status<5}">
                        <form onsubmit="return cancelOrder(this)" action="${pageContext.request.contextPath}/EditStatusServlet" method="post">
                            <input type="hidden" name="receipt_id_canceled" value="${receipt.id}">
                            <input type="hidden" name="receipt_status" value="${receipt.status}">
                            <input type="submit" value="<fmt:message key="manager_pages_receipt_list.receipt_cancel"/>">
                        </form>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </h3>
    <a href="${pageContext.request.contextPath}/manager_pages/manager_profile.jsp">Back...</a><br>
</div>
<script>
    function cancelOrder() {
        return confirm('<fmt:message key="manager_pages_receipt_list.receipt_cancel_msg"/>');
    }
</script>
</body>
</html>
