<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 30.01.2021
  Time: 14:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <title><fmt:message key="error_pages_404.tittle"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/error_style.css">
</head>
<body>
<div class="error">
    <h1><fmt:message key="error_pages_404.h1"/></h1>
    <h2><fmt:message key="error_pages-404.h2"/></h2>
    <a href="${pageContext.request.contextPath}/starting_page.jsp"><fmt:message key="manager_pages_users.back"/></a>
</div>
</body>
</html>
