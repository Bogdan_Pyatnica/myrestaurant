<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 04.02.2021
  Time: 20:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <title><fmt:message key="user_pages_my_order.tittle"/></title>
    <jsp:include page="/navigation/navigation_bar.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/table_style.css">
</head>
<body>
<h2 class="info">
    <c:out value="${sessionScope.user.name}"/>
</h2>
<div class="info">
<h3>
    <div class="topnav">
        <c:forEach items="${requestScope.statusMap}" var="status">
            <a href="${pageContext.request.contextPath}/UserOrderServlet?statusName=${status.value}">${status.value}</a>
        </c:forEach>
    </div>
</h3>

<h3 class="info">
    <table id="information" border="1" cellpadding="5" cellspacing="1" >
        <tr>
            <th><fmt:message key="manager_pages_receipt_list.receipt_id"/></th>
            <th><fmt:message key="manager_pages_receipt_list.receipt_create_time"/></th>
            <th><fmt:message key="manager_pages_receipt_list.receipt_total"/></th>
            <th><fmt:message key="manager_pages_receipt_list.receipt_status"/></th>
            <th><fmt:message key="user_pages_my_order.info"/></th>
            <th><fmt:message key="user_pages_my_order.pay"/></th>
            <th><fmt:message key="user_pages_my_order.cancel_order"/></th>
        </tr>
        <c:forEach items="${requestScope.receiptList}" var="receipt">
            <tr>
                <td>${receipt.id}</td>
                <td>${receipt.createTime}</td>
                <td>
                    <fmt:message key="user_pages_food_list.currency_coorse" var="currency_coorse"/>
                    <fmt:message key="user_pages_food_list.price"/> <fmt:formatNumber value="${receipt.total / currency_coorse}" type="currency"/>
                </td>
                <td>${receipt.statusName}</td>
                <td>
                    <form action="${pageContext.request.contextPath}/UserOrderServlet" method="post">
                        <input type="hidden" value="${receipt.id}" name="load_food_receipt_id">
                        <input type="submit" value="<fmt:message key="user_pages_my_order.submit_info"/>">
                    </form>
                </td>
                <td>
                    <c:if test="${receipt.status == 4}">
                        <form onsubmit="return payOrder(this);" action="${pageContext.request.contextPath}/UserOrderServlet" method="post">
                            <input type="hidden" value="${receipt.id}" name="receipt_id">
                            <input type="hidden" name="pay_form">
                            <input type="hidden" value="${receipt.status}" name="receipt_status">
                            <input type="submit" value="<fmt:message key="user_pages_my_order.submit_pay"/>">
                        </form>
                    </c:if>
                </td>
                <td>
                    <c:if test="${receipt.status < 5}">
                    <form form onsubmit="return msg(this);" action="${pageContext.request.contextPath}/UserOrderServlet" method="post">
                        <input type="hidden" value="${receipt.id}" name="receipt_id">
                        <input type="hidden" value="${receipt.status}" name="receipt_status">
                        <input type="hidden" name="cancel_receipt_form">
                        <input type="submit" value="<fmt:message key="user_pages_my_order.submit_cancel"/>">
                    </form>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
    </table>
</h3>
</div>
<script>
    function msg() {
        return confirm('<fmt:message key="user_pages_my_order.cancel_confirm_msg"/>');
    }
</script>
<script>
    function payOrder() {
        return confirm('<fmt:message key="user_pages_my_order.pay_confirm_msg"/>');
    }
</script>
</body>
</html>
