<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 12.02.2021
  Time: 21:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <title><fmt:message key="user_navbar.order_info"/></title>
    <jsp:include page="/navigation/navigation_bar.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/table_style.css">
</head>
<body>
<div class="info">
    <h2><fmt:message key="user_pages_receipt_info.tittle"/></h2>
    <h3>
        <table id="information" border="1" cellpadding="5" cellspacing="1" >
            <tr>
                <th><fmt:message key="manager_pages_add_new_food.name"/></th>
                <th><fmt:message key="manager_pages_add_new_food.description"/></th>
                <th><fmt:message key="manager_pages_add_new_food.price"/></th>
                <th><fmt:message key="manager_pages_add_new_food.amount"/></th>
                <th><fmt:message key="manager_pages_receipt_list.receipt_total"/></th>
            </tr>
            <c:forEach items="${requestScope.food_for_receipt}" var="food">
                <fmt:message key="user_pages_food_list.currency_coorse" var="currency_coorse"/>
                <tr>
                    <td>${food.name}</td>
                    <td>${food.description}</td>
                    <td>
                        <fmt:formatNumber value="${food.price / currency_coorse}" type="currency"/>
                    </td>
                    <td>${food.amount}</td>
                    <td>
                     <fmt:formatNumber value="${(food.amount*food.price) / currency_coorse}" type="currency"/>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </h3>
        <a href="${pageContext.request.contextPath}/UserOrderServlet"><fmt:message key="manager_pages_users.back"/></a>
</div>
</body>
</html>
