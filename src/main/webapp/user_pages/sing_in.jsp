<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 27.01.2021
  Time: 12:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>

<html lang="${param.lang}">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <title> <fmt:message key="sign_in_page.tittle"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/form_style.css">
</head>
<body class="text-center">
<main class="form-signin">
<form action="${pageContext.request.contextPath}/SignIn" method="post">
    <h2><fmt:message key="sign_in_page.form_tittle"/></h2><br>
    <b><fmt:message key="sign_in_page.insert_login"/></b><br>
    <input pattern="^(?=.{4,10}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._А-яЄЇєїґЁё]+(?<![_.])$" type="text" name="login" placeholder="Login"/><br>
    <b><fmt:message key="sign_in_page.insert_password"/></b><br>
    <input pattern="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\S+$).{4,10}$" type="password" name="password" placeholder="Password"/><br>
    <input type="hidden" name="sign_in_form">
    <input type="submit" value="<fmt:message key="sign_in_page.submit"/>"/><br>
</form>
</main>
</body>
</html>
