<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 28.01.2021
  Time: 16:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>You already singed.</title>
    <jsp:include page="/navigation/navigation_bar.jsp"/>
</head>
<body>
<div>
    <h1>Go back to menu or sing out.</h1>
    <a href="${pageContext.request.contextPath}/user_pages/greetings_sing.jsp">Back</a>
    <a href="${pageContext.request.contextPath}/SingOut">Sing Out</a>
</div>
</body>
</html>
