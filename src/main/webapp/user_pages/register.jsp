<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 26.01.2021
  Time: 19:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>

<html lang="${param.lang}">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/form_style.css">
    <title><fmt:message key="register_page.tittle"/></title>
</head>
<body class="text-center">
<main class="form-signin">
<h1><fmt:message key="register_page.insert_data"/></h1><br>
<form action="${pageContext.request.contextPath}/SaveServlet" method="post">
    <div class="info">
        <h2><fmt:message key="register_page.insert_data"/></h2><br>
    <b><fmt:message key="register_page.insert_data_name"/></b><br>
    <input pattern="^(?=.{4,15}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$" type="text" name="user_name" placeholder="User Name"/><br>
        <b><fmt:message key="register_page.insert_data_email"/></b><br>
        <input pattern="\S+@\S+\.\S+" type="text" name="email" placeholder="example@gmail.com"/><br>
    <b><fmt:message key="register_page.insert_data_login"/></b><br>
    <input  pattern="^(?=.{4,10}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._А-яЄЇєїґЁё]+(?<![_.])$" type="text" name="login" placeholder="Login"/><br>
    <b><fmt:message key="register_page.insert_data_password"/></b><br>
    <input pattern="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\S+$).{4,10}$" type="password" name="password" placeholder="pAssword123"/><br>
        <input type="hidden" name="register_form">
    <input type="submit" value="<fmt:message key="register_page.submit"/>"/><br>
    </div>
</form>
</main>
</body>
</html>
