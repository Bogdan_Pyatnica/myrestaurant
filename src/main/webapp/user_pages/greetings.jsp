<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 26.01.2021
  Time: 19:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="userinfo" uri="http://mycompany.com" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <title><fmt:message key="user_pages_greetings.tittle"/></title>
    <jsp:include page="/navigation/navigation_bar.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
</head>
<body>
<div class="info">
<h1><fmt:message key="user_pages_greetings.tittleh1"/></h1>
<h1>
    <userinfo:userInfo user="${sessionScope.user}"/><br>
</h1>
    <h2>
        <fmt:message key="user_pages_greetings.info"/> <a href="${pageContext.request.contextPath}/LoadFood"><fmt:message key="user_navbar.food_list"/></a><br>
        <a href="${pageContext.request.contextPath}/starting_page.jsp"><fmt:message key="manager_pages_users.back"/></a><br>
    </h2>
</div>
</body>
</html>
