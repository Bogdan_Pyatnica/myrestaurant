<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 07.02.2021
  Time: 11:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <title><fmt:message key="user_navbar.cart"/></title>
    <jsp:include page="/navigation/navigation_bar.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/table_style.css">

</head>
<body>
<h1><fmt:message key="user_pages_user_cart.tittle"/></h1>
<table id="information" border="1" cellpadding="5" cellspacing="1" >
    <tr>
        <th><fmt:message key="manager_pages_add_new_food.name"/></th>
        <th><fmt:message key="manager_pages_add_new_food.description"/></th>
        <th><fmt:message key="manager_pages_add_new_food.price"/></th>
        <th><fmt:message key="manager_pages_add_new_food.amount"/></th>
        <th><fmt:message key="manager_pages_receipt_list.receipt_total"/></th>
        <th><fmt:message key="user_pages_user_cart.remove"/></th>
    </tr>
    <c:forEach var="food" items="${sessionScope.food_ordered}">
        <tr>
            <fmt:message key="user_pages_food_list.currency_coorse" var="currency_coorse"/>
            <td>${food.name}</td>
            <td>${food.description}</td>
            <td>
                <fmt:formatNumber value="${food.price / currency_coorse}" type="currency"/>
            </td>
            <td>${food.amount}</td>
            <td>
                <fmt:formatNumber value="${(food.amount*food.price) / currency_coorse}" type="currency"/>
            </td>
            <td>
                <form action="${pageContext.request.contextPath}/MakeOrderServlet" method="post">
                    <input type="hidden" name="food_remove" value="${food.name}">
                    <input type="submit" value="<fmt:message key="user_pages_user_cart.remove"/>">
                </form>
            </td>
        </tr>

    </c:forEach>
        <form action="${pageContext.request.contextPath}/SubmitOrderServlet" method="post">
            <input type="hidden" name="food_ordered" value="${sessionScope.food_ordered}">
            <input type="submit" value="<fmt:message key="user_pages_user_cart.submit_order"/>">
        </form>
</table>

</body>
</html>
