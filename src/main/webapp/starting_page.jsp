<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 27.01.2021
  Time: 11:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>

<html lang="${param.lang}">
<head>
    <title><fmt:message key="starting_page.hello_tittle"/></title>
        <link rel="stylesheet" href="styles/style.css">
</head>
<body>
<div class="info">
    <h1><fmt:message key="starting_page.hello"/></h1>
    <h2>
        <fmt:message key="starting_page.chooseLang"/><br>
        <a href="${pageContext.request.contextPath}/starting_page.jsp?sessionLocale=uk"><fmt:message key="user_navbar.change_locale_uk"/></a>
        <a href="${pageContext.request.contextPath}/starting_page.jsp?sessionLocale=en"><fmt:message key="user_navbar.change_locale_en"/></a>
    </h2>
    <h2>
        <fmt:message key="starting_page.can_register"/> <a href="user_pages/register.jsp"><fmt:message key="starting_page.register"/></a>
    </h2>
    <h2><fmt:message key="starting_page.or"/></h2>
    <h2>
        <fmt:message key="starting_page.can_sign"/> <a href="user_pages/sing_in.jsp"><fmt:message key="starting_page.sign"/></a>
    </h2>
</div>
</body>
</html>
