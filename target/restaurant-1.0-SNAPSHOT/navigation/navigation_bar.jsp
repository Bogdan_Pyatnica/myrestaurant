<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 28.01.2021
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>

<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/navigation_style.css">
</head>
<body>

<div class="topnav">
    <a href="${pageContext.request.contextPath}/user_pages/user_profile.jsp"><fmt:message key="user_navbar.profile"/></a>
    <a href="${pageContext.request.contextPath}/LoadFood"><fmt:message key="user_navbar.food_list"/></a>
    <a href="${pageContext.request.contextPath}/UserOrderServlet"><fmt:message key="user_navbar.order_info"/></a>
    <a href="${pageContext.request.contextPath}/user_pages/user_cart.jsp"><fmt:message key="user_navbar.cart"/></a>
    <a href="${pageContext.request.contextPath}/SingOut"><fmt:message key="user_navbar.sign_out"/></a>

</div>
</body>
</html>
