<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 27.01.2021
  Time: 13:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <title><fmt:message key="error_pages_permission.tittle"/></title>
    <link rel="stylesheet" href="styles/error_style.css">
</head>
<body>
<div class="error">
<h1><fmt:message key="error_pages_registration.h1"/></h1>
<a href="${pageContext.request.contextPath}/starting_page.jsp"><fmt:message key="manager_pages_users.back"/></a>
</div>
</body>
</html>
