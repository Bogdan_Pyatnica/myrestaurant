<%--
  Created by IntelliJ IDEA.
  User: piatnica13
  Date: 30.01.2021
  Time: 16:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <title><fmt:message key="user_pages_food_list.tittle"/></title>
    <jsp:include page="/navigation/navigation_bar.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</head>
<body>

<h3>
    <p><fmt:message key="manager_pages_add_new_food.category"/></p>
    <div class="topnav">
        <c:forEach items="${requestScope.foodCategory}" var="category">
            <a href="${pageContext.request.contextPath}/LoadFood?foodCategoryId=${category.key}&foodSort=${requestScope.foodSort}&currentPage=${requestScope.currentPage}">${category.value}</a>
        </c:forEach>
    </div>
    <form action="${pageContext.request.contextPath}/LoadFood" method="get">
        <p>
            <label for="sort_select">Sort by:</label>
            <select name="foodSort" id="sort_select">
                <option disabled><fmt:message key="user_pages_food_list.sort_by"/></option>
                <c:forEach items="${requestScope.foodSorting}" var="sort">
                    <option value="${sort.value}">${sort.key}</option>
                </c:forEach>
            </select>
        </p>
        <input type="hidden" value="sort_select">
        <input type="hidden" value="${requestScope.foodCategoryId}" name="foodCategoryId">
        <input type="hidden" value="${requestScope.currentPage}" name="currentPage">
        <input type="submit" value="<fmt:message key="user_pages_food_list.submit_sort"/>">
    </form>
</h3>
<h2>
    <div class="info">
            <c:forEach var="i" begin="1" end="${requestScope.numOfPages}">
                <a href="${pageContext.request.contextPath}/LoadFood?currentPage=${i}&foodCategoryId=${requestScope.foodCategoryId}&foodSort=${requestScope.foodSort}">${i}</a>
            </c:forEach>
    </div>
</h2>
<div class="album py-5 bg-light">
 <div class="container">
    <div class="row">
        <c:forEach var="foods" items="${requestScope.foodArray}">
        <div class="col-md-4">
            <div class="card mb-4 box-shadow">
                <div class="card-body">
            <c:if test="${foods.amount>0}">
                    <img class="card-img-top" src="${pageContext.request.contextPath}/images/${foods.picture}" alt="${foods.picture}">
                <p>${foods.name}</p>
                <p class="card-text">${foods.description}</p>
                <div class="btn-group">
                    <p>
                        <c:choose>
                            <c:when test="${sessionScope.lang.equals('en')}">
                                <fmt:message key="user_pages_food_list.currency_coorse" var="currency_coorse"/>
                                <fmt:message key="user_pages_food_list.price"/> <fmt:formatNumber value="${foods.price / currency_coorse}" type="currency"/>
                            </c:when><c:otherwise>
                            <fmt:message key="user_pages_food_list.price"/> <fmt:formatNumber value="${foods.price}" type="currency"/>
                        </c:otherwise>
                        </c:choose>
                    </p>
                </div>
                    <form  action="${pageContext.request.contextPath}/MakeOrderServlet" method="post">
                        <input type="hidden" name="food_name" value="${foods.name}">
                        <input type="hidden" value="${requestScope.foodCategoryId}" name="foodCategoryId">
                        <input type="hidden" value="${requestScope.currentPage}" name="currentPage">
                        <input type="hidden" name="foodSort" value="${requestScope.foodSort}">
                        <input type="submit" value="<fmt:message key="user_pages_food_list.sumbit_cart"/>">
                    </form>
            </c:if>
                </div>
            </div>
        </div>
        </c:forEach>
    </div>
 </div>
</div>

</body>
</html>
